package it.etinet.tripfeatureapp.Core;

import java.util.List;

import it.etinet.tripfeatureapp.Model.Category;
import it.etinet.tripfeatureapp.Model.Channel;
import it.etinet.tripfeatureapp.Model.GeoC;
import it.etinet.tripfeatureapp.Model.GeoObject;
import it.etinet.tripfeatureapp.Model.NewMessage;
import it.etinet.tripfeatureapp.Model.News;
import it.etinet.tripfeatureapp.Model.Update;
import it.etinet.tripfeatureapp.ModelSpecial.LObjectBrand;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public interface Poison {

    @GET("/geoObject")
    void getLastPois(
            @Query("pageIndex") int pageIndex,
            @Query("pageSize") int pageSize,
            @Query("geoObjectContext") String geoObjectContext,
            @Query("format") String format,
            Callback<List<GeoObject>> listGeoObject
    );

    @GET("/geoObject")
    void getPoisByPosition(
            @Query("lat") double lat,
            @Query("lon") double lon,
            @Query("format") String format,
            Callback<List<GeoObject>> listGeoObject
    );

    @GET("/geoObject")
    void getPoiByCatAndOwner(
            @Query("catIds") String catIds,
            @Query("channelId") String channelId,
            @Query("pageIndex") int pageIndex,
            @Query("format") String format,
            Callback<List<GeoObject>> listGeoObject
    );

    @GET("/geoObject")
    void getPoiByCatAndOwnerPosition(
            @Query("catIds") String catIds,
            @Query("channelId") String channelId,
            @Query("pageIndex") int pageIndex,
            @Query("format") String format,
            @Query("lat") double lat,
            @Query("lon") double lon,
            Callback<List<GeoObject>> listGeoObject
    );


    @GET("/geoObject/{Id}")
    void getPoiById(
            @Path("Id") String poiId,
            @Query("format") String format,
            Callback<GeoObject> geoObject
    );

    @GET("/channel/{Id}")
    void getChannelById(
            @Path("Id") String channelId,
            @Query("format") String format,
            Callback<Channel> channel
    );

    @GET("/channel")
    void getChannelByPosition(
            @Query("position") String position,
            @Query("format") String format,
            Callback<Channel> channel
    );

    @GET("/channel")
    void getLastChannels(
            @Query("take") int numberOf,
            @Query("format") String format,
            Callback<List<Channel>> listChannels
    );

    @GET("/geoCoder")
    void getMyPosition(
            @Query("position") String position,
            Callback<GeoC> myPosition
    );

    @GET("/category")
    void getListCategories(
            Callback<List<Category>> category
    );

    @GET("/news?")
    void gettripfeatureappFlood(
            Callback<List<News>> callback
    );

    @GET("/news")
    void getNewsByChannelId(
            @Query("channelId") String channelId,
            @Query("format") String format,
            Callback<List<News>> listNews
    );

    @GET("/geoObject")
    void searchGeoObjectByPulse(
            @Query("query") String query,
            @Query("pageIndex") int pageIndex,
            @Query("pageSize") int pageSize,
            @Query("format") String format,
            Callback<List<GeoObject>> listGeoObject
    );

    @GET("/geoObject")
    void searchGeoObjectByProx(
            @Query("query") String query,
            @Query("pageIndex") int pageIndex,
            @Query("pageSize") int pageSize,
            @Query("lat") double lat,
            @Query("lon") double lon,
            @Query("format") String format,
            Callback<List<GeoObject>> listGeoObject
    );

    @GET("/pushMessage/{messageId}")
    void getNewMessage(
            @Path("messageId") String messageId,
            Callback<NewMessage> NewMessage
    );

    @GET("/utility?action=checkUpdate")
    void checkUpdate(
            Callback<Update> updateCallback
    );

    @GET("/pushMessage")
    void getMessages(
            @Query("channels") String channels,
            @Query("date") long date,
            Callback<List<NewMessage>> listCallback
    );

    @GET("/brand/{id}")
    void getBrandById(
            @Path("id") String brandId,
            Callback<LObjectBrand> brandCallback
    );

    @GET("/utility?action=testHeader")
    void testHeader(
            Callback<String> ok
    );
}