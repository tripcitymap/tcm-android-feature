package it.etinet.tripfeatureapp.Core;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import it.etinet.tripfeatureapp.Vars.StaticVars2;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

public enum LoneClient {

    INSTANCE;
    private final RestAdapter restClient;

    private LoneClient() {
        restClient = new RestAdapter.Builder()
                .setEndpoint(StaticVars2.meteoEndPoint)
                //.setConverter(new SimpleXMLConverter())
                .setClient(new OkClient(client()))
                .build();
    }

    public RestAdapter getClient() {
        return restClient;
    }

    private OkHttpClient client () {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(10, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(15, TimeUnit.SECONDS);
        return okHttpClient;
    }
}
