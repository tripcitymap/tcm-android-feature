package it.etinet.tripfeatureapp.Core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import it.etinet.tripfeatureapp.Vars.StaticVar;
import it.etinet.tripfeatureapp.Vars.StaticVars2;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

public enum SingletonRestClient {

    INSTANCE;
    private final Poison restClient;

    private SingletonRestClient() {
        restClient = new RestAdapter.Builder()
                .setEndpoint(StaticVars2.reqUrl)
                .setConverter(new JacksonConverter(new ObjectMapper()))
                .setRequestInterceptor(requestInterceptor)
                .setClient(new OkClient(client()))
                .build()
                .create(Poison.class);
    }

    public Poison getRestClient() {
        return restClient;
    }

    RequestInterceptor requestInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addHeader("TCM-ClientID", StaticVars2.android_id);
            request.addHeader("TCM-ClientVersion", StaticVar.appVersion);
            request.addHeader("TCM-ClientOS", "Android");
            if("it".equals(StaticVar.devicelang)) {
                request.addHeader("Accept-Language", "it-IT");
            }else {
                request.addHeader("Accept-Language", "en-GB");
            }
        }
    };

    private OkHttpClient client () {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(10, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(15, TimeUnit.SECONDS);
        return okHttpClient;
    }
}