package it.etinet.tripfeatureapp.Vars;


import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ProgressBar;

import com.google.android.gms.maps.GoogleMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.etinet.tripfeatureapp.Model.Category;
import it.etinet.tripfeatureapp.Model.Channel;
import it.etinet.tripfeatureapp.Model.DBObject;
import it.etinet.tripfeatureapp.Model.ExternalComunications;
import it.etinet.tripfeatureapp.Model.GeoObject;
import it.etinet.tripfeatureapp.Model.News;
import it.etinet.tripfeatureapp.Model.PushCentroNotifiche;
import it.etinet.tripfeatureapp.Model.mCategory;

public class StaticVars2 {

    public static String testNotifId;
    public static boolean fromPushCentro;

    public static String flagId;
    public static String tagId;

    public static boolean moreResultsDoneMap = true;
    public static boolean openDirectPush;
    public static boolean fromListMap;

    public static ExternalComunications externalComunication;

    public static ProgressBar pbLoadingMoreResultsMap;

    public static boolean fromBacheca;
    public static boolean fromPushNotif;
    public static boolean fromTripCityLife;
    //
    public static News actualNews;
    //
    public static boolean fromPref;
    public static boolean fromCr;
    public static boolean fromComune;
    public static String mTestString ;
    public static long updateDashTimer;

    public static boolean inItaly = true;
    //
    public static String urlFotoIlMioComune;
    public static String idComunePref;
    public static boolean mapWithMenuActivity;
    //
    public static String newChannelImage;
    public static boolean checkImage = false;
    //
    public static boolean collapsedFromSearch;
    public static boolean queryAutoSearch;
    //
    public static boolean imPulseSearch = true;
    public static boolean imProxSearch;
    public static String channelType;
    public static int countTripcityLifeToast;
    //
    public static boolean imFake;
    public static boolean unable;
    public static PushCentroNotifiche actualPush;

    //
    public static String imVisiting;
    public static String android_id;

    //
    public static float density;

    //
    public static List<News> listTripCityLife;

    public static AsyncTask<Void, Void, Void> asyncTaskMorePoi;
    
    //Pageindex
    public static int pageIndexMap = 0;
    public static int pageIndexMapSearch = 0;

    public static  AsyncTask<Void, Void, Void> getListaPoi;
    public static  AsyncTask<Void, Void, Void> getListaPoi1;
    public static  AsyncTask<Void, Void, Void> getListaPoi2;
    public static  AsyncTask<Void, Void, Void> getListaPoi3;
    public static  AsyncTask<Void, Void, Void> getListaPoi4;

    //News
    public static String genericNewsImages;
    public static String idGenericoComuneNews;

    public static ArrayList<GeoObject> listLastPoiPremium;
    public static List<String> listDistanceToMePremium ;
    public static List<String> listNumFotoPremium ;
    public static List<String> listUrlPremium ;

    //Poi Dash Natura
    public static ArrayList<GeoObject> listLastPoiNature;
    public static List<String> listUrlNature;
    public static List<String> listNumFotoNature;
    public static List<String> listNamesNature;
    public static List<String> listCatNature;

    //Poi Dash Art
    public static ArrayList<GeoObject> listLastPoiArt;
    public static List<String> listUrlArt;
    public static List<String> listNumFotoArt;
    public static List<String> listNamesArt;
    public static List<String> listPositionArt;
    public static List<String> listCatArt;

    //Select on a poi from DashBoard
    public static int tappedPoiPremium;
    public static int tappedPoiCulture;
    public static int tappedPoiArt;

    // General boolean checks
    public static boolean imPremium;
    public static boolean imNature;
    public static boolean imArt;
    public static boolean imPoiFromMap;

    //ListCat&Sub
    public static List<Category>listFullCategoryStatic;

    //Merging 2 bitmaps
    public static int mergeScreenWidth;
    public static int mergeScreenHeight;

    //sErrorCode
    public static boolean sErrorCode;

    //
    public static boolean onDestroy = false;
    public static boolean urgentRestart = false;

    public static boolean italianMode = true;

    //Position Things
    public static String nelComuneDi;
    public static String meteoEndPoint = "http://weather.yahooapis.com/forecastrss?w=";
    //public static String reqUrl = "http://coreservice.localhost.tripfeatureapp.com/1.5/";
    public static String reqUrl = "http://coreservice.tripfeatureapp.com/1.5/";
    //public static String zippy = "https://portalvhdsskwc7p9xzvxz2.blob.core.windows.net/tcm-resources/tcm_icons.zip";
    public static String zippy = "https://portalvhdsskwc7p9xzvxz2.blob.core.windows.net/tcm-resources/tcm_icons_test.zip";
    //Request Stuffs type: {cat, channel, geoObject, news, push, geocoder}
    public static String tCat = "category";
    public static String tChannel ="channel?";
    public static String tGeoObject = "geoObject?";
    public static String tNews = "news?";
    public static String tPush ="pushMessage/";
    public static String tGeocoder = "geocoder?";
    public static String tSearch = "geoObject?";


    public static boolean fromAutocomplete;

    //Last channels
    public static List<Channel> listLastChannels;
    public static List<String>  listUrlChannels;
    public static List<String>  listTotPoiChannels;
    public static List<String>  listDistanceToMeChannels;
    public static List<String>  listNamesChannels;

    //About the user positions
    public static double userLat;
    public static double userLon;

    //About il mio comune
    public static String totalPoiIlMioComune;
    public static  boolean iWereOutForGPS;
    //IL mio comune
    public static List<mCategory> listCatSubIlmioComune;
    public static String subIdCat;
    public static String idCat;
    public  static Map<String, String> listCatMap;
    public  static Map<String, String>  listSubCatMap;

    //Tests
    public static List<GeoObject> listRes;
    public static String [] catId = new String[]{"101" , "201"};

    //map with menu
    public static List<GeoObject> listPoiMapWithMenu;
    public static int lenghtListRes;
    public  static  List<GeoObject> listaPoiVicini;
    public static int tappedPoi;
    public static String idcategorietapped;
    public static String namecomuneTapped;

    //Testing the balloon stuffs

    public static String balloonFatherId;
    public static GeoObject objInfoMap;

    //comune form dash board
    public static String ownerComuneFromDash;
    public static String nomeComuneFromDash;

    //var expandiblelistadapter
    public static List<String> idSubCategory;
    //variabile di provinienza
    public static boolean comuneilMIoComune;
    public static boolean comuneDashBoard;
    public static boolean comunePosizione;
    public static boolean comuneRicerca;

    //Meteo Vars Comune
    public static String comunePosizioneTemperaturaFComune;
    public static String comunePosizioneCodeTemperaturaComune;
    public static String comunePosizioneTxtMeteoGradiComune;
    public static String comunePosizioneTxtMeteoDescriptionComune;
    public static Drawable comunePosizioneImagemeteoComune;

    public static String comuneGeneralTemperaturaFComune;
    public static String comuneGeneralCodeTemperaturaComune;
    public static String comuneGeneralTxtMeteoGradiComune;
    public static String comuneGeneralTxtMeteoDescriptionComune;
    public static Drawable comuneGeneralImagemeteoComune;

    //Vars ID comune
    public static String idComuneIlMioComune = "53f5bbfd2719391808e6fd13"; // idComuneIlMioComune
    public static String idComuneDashBoard;
    public static String idComunePosizione;
    public static String idComuneRicerca;

    //Total Pois
    public static String totalPoiComunePosizione;
    public static String totalPoiComuneDashBoard;

    //Covered
    public static boolean enteredWithPosition = true;

    //General Image
    public static String urlImmagineDashboardUsedInComune;

    //Get News Bacheca
    public static List<News> listTripCityLifeBacheca;
    public static List<String> urlimageNewsBacheca;
    public static boolean scrolledBacheca=false;
    public static List<String> urlimageComuneBacheca;
    public static List<String> urlimageComune;
    public static int countTripcityLifeBacheca=10;
    public static int tappedBacheca;

    public static List<String> urlimageNews;

    public static List<String> nameTripCityComuni;

    //DashBoardLastChannels tapped id
    public static String tappedDashBoardChannelId;

    //Single Map GeoObject
    public static GeoObject singleMapGeoObject;
    //schermo util
    public static int screenWidth;
    public static int screenHeight;

    //Ricerca
    public static String nomeComuneRicerca;
    public static String fotoComuneRicerca;
    public static String totalPoiComuneRicerca = "";

    //city radar
    public static List<GeoObject> listPoiCityRadar;
    public static List<String> filtercategoriedavedere;
    public static GoogleMap mMap;
    public static boolean satelliteModeOn;
    public static boolean stradeModeOn;
    public static boolean duedModeOn;
    public static boolean tredModeOn;

    //Bacheca fragment needed Obj
    public static GeoObject bachecaFragmentObj;

    //Database object needed for news
    public static DBObject comuniNewsStaticDBObject;
    public static String nomeComuneNews = "";

    //Variables used for the search in database;
    public static String searchedDBName;
    public static String searchedDBID;
    public static String searchedDBPhoto;
    public static String searchedDBTotalPoi = "";
    public static String searchCityCenter;
    public static String searchCat;

    //preferity var
    public static List<String> nomeComunePreferity;
    public static List<String> ownerComunePreferity;
    public static List<String> imageComunePreferity;
    public static List<String> totalPoiComunePreferity;
    public static boolean mioComuneDirisidenzafromPref;
    public static boolean activeCityPref;
    public static boolean activePoiPref;
    public static boolean menoPref;
    public static boolean eroINPref;
    public static boolean IamPOiPreferity;
    public static List<String> listPreferityPoi;
    public static int nbPoiPref;
    public static int nbChannelPref;
    public static List<GeoObject> poiGeoObjectPref;
    //General Stuffs

    public static String ricercaDBId;
    public static String ricercaDBimmagine;

    //
    public static String updateCityId;

    //
    public static GeoObject poiImages;
    public static String query;

    public static String [][] strServizi = {
            {"se_a0","Turismo","Tourism"},
            {"se_a1","Panorama","Panorama"},
            {"se_a2","Cambio valuta","Currency exchange"},
            {"se_a3","Visite guidate","Guided Tours"},
            {"se_a4","Multilingua","Multilanguage"},
            {"se_b1","Accesso disabili","Disabled people's access."},
            {"se_b2","Animali ammessi","Pets welcome"},
            {"se_b3","Prodotti senza glutine","Gluten-free products"},
            {"se_b4","Prodotti senza lattosio","Lactose-free products"},
            {"se_b5","Prodotti per vegetariani","Products for vegetarians"},
            {"se_b7","Area fumatori","Smoking area"},
            {"se_c1","Bar e bevande","Coffee and drinks"},
            {"se_c2","Ristorante","Restaurant"},
            {"se_c3","Apericena e Brunch","Buffet and brunch"},
            {"se_d1","Parcheggio privato","Private parking"},
            {"se_d2","Carro attrezzi","Tow truck"},
            {"se_d3","Autolavaggio","Car wash"},
            {"se_d4","Servizio Navetta","Shuttle service"},
            {"se_d5","Gpl","Gpl / Lpg"},
            {"se_d6","Metano","Natural gas"},
            {"se_d7","Traghetto","Ferry boat"},
            {"se_d8","Bus","Bus"},
            {"se_d9","Tram","Tramcar"},
            {"se_d10","Cicloturismo","Cycling"},
            {"se_d11","Equitazione","Horseback riding"},
            {"se_d12","Elipista","Helipad"},
            {"se_d13","Security","Security"},
            {"se_d14","Officina meccanica","Mechanical workshop"},
            {"se_e1","SPA e centro benessere","SPA and wellness center"},
            {"se_e2","Area verde","Green area"},
            {"se_e3","Area bimbi","Kids area"},
            {"se_e4","Locale climatizzato","Air-conditioned room"},
            {"se_e5","Intrattenimento musicale","Live music"},
            {"se_e6","Animazione","Entertainment"},
            {"se_e7","Dehor","Dehor"},
            {"se_e8","Area attrezzata","Equipped areas"},
            {"se_e9","Free-camping","Free-camping"},
            {"se_e10","Attrezzature balneari","Bathing equipments"},
            {"se_f1","Camere","Rooms"},
            {"se_f2","Guardaroba","Wardrobe"},
            {"se_f3","Cassetta di sicurezza","Safe-deposit box"},
            {"se_f4","Ricarica elettrica veicoli","Electric plug-in"},
            {"se_f5","Scarico acque reflue","Wastewater dump"},
            {"se_g1","Bancomat","ATM"},
            {"se_g2","Shop","Shop"},
            {"se_g3","Minimarket","Minimarket"},
            {"se_h1","Escursionismo","Trekking"},
            {"se_h2","Fauna protetta","Protected wildlife"},
            {"se_h3","Flora protetta","Protected plant species"},
            {"se_h4","Balneazione","Bathing"},
            {"se_h5","Arrampicata","Climbing"},
            {"se_i1","Posta","Post office"},
            {"se_i2","Wi-fi","Wi-fi"},
            {"se_i3","Sala riunioni","Meeting room"},
            {"se_l1","WC","WC"},
            {"se_l2","Punto di soccorso","Rescue point"},
            {"se_l3","Doccia","Shower"},
            {"se_l12","Defibrillatore","Defibrillator"},
            {"se_m1","Apertura serale","Night opening"},
            {"se_m2","Aperto 7 su 7","7 days a week opening"},
            {"se_m3","Luogo riparato","Covered place"},
            {"se_m4","Ingresso a pagamento","Fee required"},
            {"se_n1","Palestra","Gym"},
            {"se_n2","Piscina","Pool"},
            {"se_n3","Campo da calcio","Football field"},
            {"se_n4","Campo da basket","Basketball court"},
            {"se_n5","Campo da tennis","Tennis court"},
            {"se_n6","Ping pong","Table tennis"},
            {"se_n7","Campo da Pallavolo","Volleyball court"},
            {"se_n8","Pista da sci","Ski run"},
            {"se_n9","Pista ciclabile","Bicycle path"},
            {"se_n10","Campo da Beach volley","Beach volley court"},
            {"se_n11","Pista di atletica","Athletics field"},
            {"se_n12","Percorso asfaltata","Paved track"},
            {"se_n13","Percorso sterrata","Unpaved track"},
            {"se_n14","Pista da ballo","Dance floor"},
            {"se_n16","Area di pesca","Fishing area"},
            {"se_n17","Campo da rugby","Rugby field"},
            {"se_n18","Campo d'equitazione","Horseback riding field"},
            {"se_n19","Capo da bocce","Bowling green"}
    };

    //Search
    public static boolean fromSearch;

    public static String deviceCountry;

    public static String [] imagesUrls;

    public static GeoObject prefObj;

    public static boolean svgDet;

    public static Channel testMainChannel;
}