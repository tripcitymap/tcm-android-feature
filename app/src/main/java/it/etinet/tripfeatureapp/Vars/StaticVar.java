package it.etinet.tripfeatureapp.Vars;


import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.Polyline;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import it.etinet.tripfeatureapp.Model.PushCentroNotifiche;

public class StaticVar {

    public static boolean panoramioProblems = false;
    //Meteo
    public static boolean meteoProblems = false;
    //Meteo download
    public static boolean meteoDownProb = false;

    public static String appMode = "prod";

    public static boolean needTrim = false;
    public static boolean internet = true;
    public static  String comune = null;
    public static  String url = "http://service.tripfeatureapp.com/1.0/";
    public static  String owner;

    public static Boolean coperturaPosizione = false;

    public static String newname;

    public static int posizioneGonfAziende;
    public static boolean isOk;
    public static Bitmap bckAziende;

    public static boolean fromRadar;
    public static boolean fromNews;

    public static Double latitude = 0d;
    public static Double longitude = 0d;

    public static String iscomune  = "user?iscomune=true" ;
    public static String iscompany = "user?iscompany=true";

    public static String devicelang = "it"; // Default Language is Italian.

    public static byte numberofcomuni = 2;
    public static byte numberofpoi = 6;

    public static boolean notPosition = false;

    public static String comune_residenza;

    public static long idCat;

    public static boolean isCovered;

    public static long subIdCat;

    public static String lontest = null;
    public static String lattest = null;
    public static String d1="HCykwlwqRzJY";

    //Meteo
    public static String txtMeteoDescription;
    public static String txtMeteoGradi;
    public static Drawable imagemeteo;
    public static Drawable imagemeteoComune;

    //Added Display metricts static vars
    public static int widthPixels;
    public static int heightPixels;

    public static float widthDp;
    public static float heightDp;

    public static  String temperaturaF;
    public static  String codeTemperatura="3200";

    public static String[][] weatherIcon = {{"0","tornado","Tornado","Tornado","Tornado","h1"},
            {"1","tropical storm","Tempesta tropicale","Rainstorm","Tempesta","c3"},
            {"2","hurricane","Uragano","Hurricane","Uragano","h1"},
            {"3","severe thunderstorms","Forti Temporali","Thunderstorms","Temporali","c3"},
            {"4","thunderstorms","Temporali","Thunderstorms","Temporali","c3"},
            {"5","mixed rain and snow","Pioggia mista Neve","Sleet","Pioggia e neve","e1"},
            {"6","mixed rain and sleet","Pioggia e Nevischio","Sleet","Nevischio","e2"},
            {"7","mixed snow and sleet","Nevischio","Sleet","Nevischio","e2"},
            {"8","freezing drizzle","Pioggerella gelata","Freezing rain","Pioggia gelata","e2"},
            {"9","drizzle","Pioggerella","Drizzle","Pioggia debole","c1"},
            {"10","freezing rain","Pioggia gelata","Freezing rain","Pioggia gelata","e2"},
            {"11","showers","Rovescio","Rain","Pioggia","c2"},
            {"12","showers","Revescio","Rain","Pioggia","c2"},
            {"13","snow flurries","Nevischio","Sleet","Nevischio","e2"},
            {"14","light snow showers","Leggeri nevicate","Light snow","Neve debole","e3"},
            {"15","blowing snow","Raffiche di neve","Blowing snow","Nevischio","e2"},
            {"16","snow","Neve","Snow","Neve a tratti","e3"},
            {"17","hail","Grandine","Hail","Grandinate","d1"},
            {"18","sleet","Nevischio","Sleet","Nevischio","e2"},
            {"19","dust","Polvere","Dust","Polvere","f1"},
            {"20","foggy","Nebbia","Foggy","Nebbia","f2"},
            {"21","haze","Foschia","Haze","Foschia","f1"},
            {"22","smoky","Nebbioso","Smoky","Velato","f1"},
            {"23","blustery","Tempesta","Blustery","Tempesta","c4"},
            {"24","windy","Ventoso","Windy","Vento forte","g1"},
            {"25","cold","Freddo pungente","Cold","Gelido","e5"},
            {"26","cloudy","Nuvoloso","Cloudy","Nuvoloso","b2"},
            {"27","mostly cloudy (night)","Molto Nuvoloso (notte)","Mostly cloudy","Nuvoloso","b2"},
            {"28","mostly cloudy (day)","Molto Nuvoloso (giorno)","Mostly cloudy","Nuvoloso","b2"},
            {"29","partly cloudy (night)","Parzialmente Nuvoloso (notte)","Partly cloudy","Nubi sparse","b1"},
            {"30","partly cloudy (day)","Parzialmente Nuvoloso (giorno)","Partly cloudy","Nubi sparse","b1"},
            {"31","clear (night)","Sereno (notte)","Clear","Sereno","a1"},
            {"32","sunny","Soleggiato","Sunny","Soleggiato","a1"},
            {"33","fair (night)","Sereno (notte)","Fair","Sereno","a1"},
            {"34","fair (day)","Sereno (giorno)","Fair","Sereno","a1"},
            {"35","mixed rain and hail","Neve e Grandine","Rain and hail","Nevischio","e2"},
            {"36","hot","Caldo e soleggiato","Hot","Molto caldo","a2"},
            {"37","isolated thunderstorms","Temporali isolati","Thunderstorms","Temporali","c3"},
            {"38","scattered thunderstorms","Temporali sparsi","Thunderstorms","Temporali","c3"},
            {"39","scattered thunderstorms","Temporali sparsi","Thunderstorms","Temporali","c3"},
            {"40","scattered showers","Rovesci sparsi","Showers","Pioggia","c2"},
            {"41","heavy snow","Intense Nevicate","Heavy snow","Forti nevicate","e4"},
            {"42","scattered snow showers","Neve a tratti","scattered snow","Neve a tratti","e3"},
            {"43","heavy snow","Intense Nevicate","Heavy snow","Forti nevicate","e4"},
            {"44","partly cloudy","Parzialmente coperto","Partly cloudy","Nubi sparse","b1"},
            {"45","thundershowers","Acquazzone","Rainstorm","Forte pioggia","c3"},
            {"46","snow showers","Rovesci di neve","Snow showers","Nevicate","e3"},
            {"47","isolated thundershowers","Acquazzoni isolati","Rainstorm","Forte pioggia","c3"},
            {"3200","not available","non disponibile","n.a.","n.d.","n.a."},};

    //button go subcategorie selection

    public  static View buttonGo;
    public static Menu menu1;

    public static String nomeComuneNews;
    public static String descrizioneComuneNews;

    public static Long ownerIdNews;

    public static Bitmap imgbackground;
    //
    public static boolean fromcanal;

    public static String a1 = "nmwwEwl9qus";
    public static String f1a="vQIQBo";

    //
    public static boolean fromcategoriefragment=false;
    //List cat_subCat per la richiesta al service
    public  static Map<String, Long> listCatMap;
    public  static Map<String, Long>  listSubCatMap;
    public static String b1="Tg3y6Tp2B4ITmQAf2CZAS7";
    public static ArrayList<String> selectedNames;

    //TripCityLife PageIndex and PageSize
    //public static List<TripCityLife> listTripCityLife=null;
   // public static List<String> urlimageNews;
    public static List<Bitmap> listImageNews;

    public static int countTripcityLife = 0;
    public static boolean scrolled=false;
    public static FrameLayout buttonPiu;
    public static AsyncTask<Void, Void, Void> asyncTask;

    public static boolean moreRecords;

    public static boolean thereAreNews;

    // public static int R.drawable.cat_121;
   // public static List<String> urlimageComune;
    public static List<Bitmap> listImageComune;
    public static String c1="JtOxufmX6TRw3Lwytt";
    //variabile per la mapa with menu
    public  static  boolean fromdashboard = true;
    public static int tappedPoi;

    //cancellare iterinario
    public static Polyline line = null;
    public static int pageSizePrimaCat = 10;
    public static int pageSizeAltriCat;
    public static int addPrimaCat;
    public static String e1="CKxYNfvvb0s";
    public static int addAltriCat;
    public static AsyncTask<Void, Void, Void> asyncTaskAltriPoi;

    public static int lenghtListRes;
    public static int  lastlengh=0;
    public static ExpandableListView expList;
    public static FrameLayout container_go;

    public static boolean deviceMdpi = false;
    public static boolean deviceHdpi = false;
    public static boolean deviceXhdpi = false;
    public static boolean deviceXxhdpi = false;

    public static String terms ="Che cos’è Trip City Map?\n\n" +
            "Trip City Map (in seguito anche TCM oppure APP) è il software applicativo sviluppato da Etinet Srl che consente agli utenti di ricercare, individuare e prendere contatto con i punti di interesse contenuti in un database e localizzati in relazione alla rispettiva posizione geografica.\n" +
            "Responsabilità A) in ordine al funzionamento del software applicativo e B) ai dati in esso contenuti - esclusioni\n" +
            "A) Etinet Srl non è responsabile in ordine a difetti di funzionamento del software applicativo TCM ovvero ad interruzioni o limitate erogazioni del servizio dallo stesso offerto, quando queste discendano da cause non dipendenti da Etinet medesima. In particolare Etinet non risponde dei disservizi determinati dal mancato e/o limitato funzionamento dei Satelliti GPS o della rete di telecomunicazione mobile (GSM, GPRS, UMTS, Etc.) o della rete fissa, dell’assenza di copertura di rete, dell’impossibilità e della difficoltà di geolocalizzazione dell’utente e della mancata copertura territoriale. Etinet Srl non è inoltre responsabile verso l’utente o verso terzi per qualsivoglia danno, patrimoniale e non patrimoniale, derivante da impossibilità d’uso del software applicativo, smarrimento di dati, perdita di profitti e/o interruzioni di attività, ove non dipendenti da dolo o responsabilità grave di Etinet medesima.\n" +
            "\n\n" +
            "B) Etinet Srl non è in alcun modo responsabile dei dati inseriti dagli utenti ovvero da terzi (ad es. banner), del mancato aggiornamento delle mappe ed in generale dei dati oggetto di ricerca, ovvero della loro inesattezza, né potrà essere chiamata a rispondere dei contenuti inseriti da parte degli utenti o da questi condivisi su TCM. La responsabilità in ordine all’inserimento di contenuti non consentiti dalla legge, ovvero la diffusione e la riproduzione degli stessi, grava in via esclusiva in capo agli utenti che hanno caricato o condiviso il materiale ovvero lo hanno illecitamente diffuso. Nessun controllo circa i contenuti (testi, fotografie, notifiche push, ecc) verrà svolto, né potrà essere richiesto a Etinet Srl, la quale dichiara sin d’ora che non svolgerà alcuna attività di moderazione. L’Utente acconsente di usufruire di TCM esclusivamente per finalità conformi alle leggi vigenti. All’Utente è fatto divieto di adottare qualsiasi comportamento e utilizzare TCM in modo fraudolento, illegale, dannoso, minaccioso, offensivo, molesto, diffamatorio, volgare, osceno, sessualmente esplicito, profanatorio, astioso, discriminatorio o comunque contrario all’ordinamento giuridico. Etinet Srl si riserva la facoltà di rimuovere il materiale inserito dagli utenti, ove questo risulti pregiudizievole agli interessi e al buon nome dello sviluppatore del software, ovvero lo stesso risulti comunque contrario alla legge, al buon costume, all’ordine pubblico. Etinet Srl si riserva, a proprio insindacabile giudizio, il diritto di revocare la Licenza d’Uso per l’utilizzo del software qualora lo stesso sia utilizzato in maniera fraudolenta, non conforme alle Leggi e comunque in modo da arrecare pregiudizio e/o danno a Etinet Srl e/o a terzi. Etinet Srl si riserva il diritto a proprio insindacabile giudizio di disattivare o sospendere l’account o l’utilizzo del Software da parte dell’Utente; e di inoltrare alle autorità competenti tale materiale e/o informazioni offensivi relativi a detto comportamento non ammesso. Gli utenti si impegnano a mantenere indenne Etinet da qualsiasi obbligo o responsabilità, incluse le eventuali spese legali sostenute per difendersi in giudizio, ovvero stragiudiziali, che dovessero sorgere a fronte di danni provocati ad altri utenti o a terzi, relativamente ai contenuti caricati online, ovvero alla violazione di norme di legge o dei termini delle presenti condizioni di servizio.\n" +
            "Modalità di utilizzo di TCM - Limitazioni\n\n" +
            "L’utente può utilizzare TCM esclusivamente per mezzo del Software e dei sistemi operativi autorizzati. Etinet Srl non sarà ritenuto responsabile dell'eventuale non compatibilità dell'apparecchio o del download di una versione non corretta del Software per lo stesso apparecchio. \n" +
            "L'utente prende atto che non è consentito: \n\n" +
            "(a) cedere in sublicenza, vendere, assegnare, disassemblare, decompilare o comunque tentare di scoprire il codice sorgente del software, affittare, noleggiare, esportare, importare, distribuire o trasferire o comunque concedere diritti sul software a terzi; \n" +
            "(b) intraprendere, causare, consentire o autorizzare la modifica, la creazione di lavori derivati o di miglioramenti, la traduzione, il reverse engineering, la decompilazione, il disassemblaggio, la decodifica, l'emulazione, l'hacking, la ricerca o il tentativo di ricerca del codice sorgente o dei protocolli del software o di qualsiasi parte o funzionalità; \n" +
            "(c) rimuovere, oscurare o alterare qualsiasi avviso di copyright o altri avvisi proprietari inclusi nel software; \n" +
            "(d) Utilizzare il Software o far sì che lo stesso (o una sua parte) venga utilizzato all'interno di prodotti o servizi commerciali a terze parti o per fornire gli stessi prodotti o servizi. \n" +
            "(e) utilizzare il Software se non tramite il proprio account utente, fatte salve le finalità di download e installazione. \n" +
            "Aggiornamenti\n\n" +
            "E’ onere dell’utente di TCM provvedere all’installazione degli aggiornamenti e delle release messe a disposizione da parte di Etinet.\n" +
            "Lo sviluppatore declina ogni responsabilità per perdite di dati o violazioni di sicurezza qualora l’utente abbia omesso, pur disponendo della versione aggiornata, di provvedere alla sua installazione.\n" +
            "Costi di connessione e di traffico\n\n" +
            "I costi di connessione e di traffico dati sulla rete, nonché quello telefonico, anche derivante dal servizio di chiamata rapida accessibile da TCM, discendono dalle condizioni del contratto sottoscritto dall’utente medesimo con il proprio operatore telefonico. \n" +
            "Qualsivoglia eventuale addebito supplementare, anche connesso ad eventuali servizi in roaming tra operatore di rete mobile, è a carico esclusivo dell’utente.\n" +
            "TCM non sostituisce il servizio telefonico.\n\n" +
            "Tramite la APP non sarà possibile effettuare chiamate d’emergenza.\n\n" +
            "L’Utente che digiti uno di tali numeri servendosi di TCM non sarà collegato ad un addetto a chiamate di emergenza, e nessun addetto a tali chiamate sarà in grado di visualizzare il numero del chiamante o la località ove lo stesso si trova.\n" +
            "Proprietà di TCM – Copyright\n\n" +
            "Il software Trip City Map è di proprietà esclusiva di Etinet Srl.\n\n" +
            "Il marchio figurativo e nominativo Trip City Map è registrato e depositato (CN2013C000407), ed è di proprietà esclusiva di Etinet Srl.\n" +
            "Il software e il marchio Trip City Map sono protetti dalle leggi, anche internazionali, vigenti in materia di diritto d’autore e proprietà intellettuale. Qualsiasi eventuale utilizzo per scopi commerciali o comunque qualsiasi uso non autorizzato del software applicativo Trip City Map o del relativo marchio, ovvero la riproduzione e/o distribuzione del software non conforme alle presenti condizioni generali, sono severamente vietati e verranno perseguiti civilmente e penalmente.\n" +
            "Il marchio Trip City Map, in ogni caso, potrà essere utilizzato, previa autorizzazione scritta di Etinet Srl, solo unitamente al software Trip City Map.\n" +
            "Ogni eventuale diverso utilizzo del marchio è soggetto ad autorizzazione scritta di Etinet Srl. \n\n" +
            "La duplicazione, la copia, la vendita o lo sfruttamento di qualsiasi porzione di Trip City Map, senza autorizzazione scritta di Etinet Srl, sono da considerarsi assolutamente vietate.\n" +
            "Non è inoltre consentito copiare, modificare o alterare in qualsiasi modo e con qualsivoglia tecnica i contenuti forniti da Trip City Map.\n" +
            "Non è consentito affittare, licenziare o sub licenziare Trip City Map.\n\n" +
            "Proprietà dei punti di interesse e dei banner - Database\n\n" +
            "I punti di interesse inseriti su TCM da parte di utenti e di terzi sono di proprietà esclusiva dell’utente che ha inserito tali dati, il quale avvalendosi del software Trip City Map dichiara di disporre delle autorizzazioni al trattamento e alla cessione degli stessi, precisando che ogni dato dallo stesso inserito è stato regolarmente acquisito nel pieno ed integrale rispetto della normativa in materia di privacy; contestualmente l’utente autorizza Etinet Srl al trattamento dei predetti dati per finalità statistiche, ovvero di marketing e/o pubblicitarie (a scopo esemplificativo e senza pretesa di esaustività: flyer, notifiche push, newsletter, social network, ecc.) ovvero a scopo di cessione degli stessi a terzi. \n" +
            "Il materiale contenuto in eventuali banner è di proprietà del titolare dell’account dal quale il banner è stato installato.\n" +
            "Modifica delle condizioni generali\n\n" +
            "Le presenti Condizioni di utilizzo possono essere soggette a modifica in qualsiasi momento senza preavviso.\n\n" +
            "Detta modifica si intende entrare in vigore immediatamente all’atto della pubblicazione. \n\n" +
            "L’utilizzo da parte del’utente, successivamente alla pubblicazione delle modifiche, costituisce accettazione senza riserva delle nuove condizioni.\n" +
            "Privacy\n\n" +
            "I dati relativi alla geolocalizzazione dell’utente potranno essere utilizzati a scopi meramente statistici.\n\n" +
            "Con l’utilizzo di Trip City Map l’utente dichiara di aver letto l’informativa ex art. 13 D. L. 196/2003 e successive modifiche intervenute, da ritenersi parte integrante delle presenti condizioni generali e di essere pertanto stato informato circa l’utilizzo e il trattamento dei dati personali da parte di Etinet Srl.\n" +
            "Cessione del contratto\n\n" +
            "Il Titolare si riserva il diritto di trasferire, cedere, subappaltare tutti o alcuni dei diritti o obblighi derivanti dai presenti Termini, senza pregiudizio per i diritti dell’utente.\n" +
            "L’utente non potrà trasferire o cedere in alcun modo i propri diritti o obblighi ai sensi dei presenti termini senza l’autorizzazione scritta di Etinet Srl.\n" +
            "Garanzia\n\n" +
            "Etinet non garantisce che TCM e i dati in esso contenuti siano del tutto esenti da errori o che la APP funzioni senza che intervengano perdite nel pacchetto dati o interruzioni, né garantisce la connessione a o la trasmissione dati a mezzo Internet. \n" +
            "Il software TCM si intende fornito “come disponibile”.\n\n" +
            "Legge applicabile e foro competente\n\n" +
            "Le presenti Condizioni generali sono soggette e si informano alle leggi italiane. \n\n" +
            "Esse si applicano anche a qualsiasi versione, release e aggiornamento successivi di TCM.\n\n" +
            "Competente per ogni controversia in ordine all’interpretazione, esecuzione e validità del presente contratto è, in via esclusiva e nei limiti della legislazione applicabile, il Foro di Cuneo.\n";



    public static String esploroComune;

    //for other actgivity
    public static String txtMeteoDescription_other;
    public static String txtMeteoGradi_other;
    public static Drawable imagemeteo_other;
    public static String temperaturaF_other;
    public static String codeTemperatura_other;
    public static String totalPoi_il_mioComune;
    public static String totalPoi_il_mioComune_esplora;
    public static String nome_il_mio_comune_esplora;
    public static String oldOwner;
    public static String totalPoi_esplora;
    public static String nome_il_mio_comune_activity;
    public static Bitmap foto_il_mio_comune_activity;

    public static boolean fromMenu=true;
    public static boolean iamindashboard=true;
    public static ExpandableListView expListView1;
    public static LinearLayout container11;

    //defrenza tra comune e azienda
    public static boolean cittanotazienda=true;
    public static ViewPager pager_esplora;

    public static String totalPoi_azienda;
    public static String nome_azienda;

    public static boolean fromesplora=false;
    public static int tappedChannel;
    public static List<Bitmap> listImageChannel;

    public static Double centre_latitude;
    public static Double centre_longitude;
    public static String ownerchannaldhash;


    //TripCityLife PageIndex and PageSize
    public static int tappedLife;




    public static Double latitude_other;
    public static Double longitude_other;
    public static List<Bitmap> listImagegofalon;
    public static List<Bitmap> listImagegofalonAzienda;


    public static int countTripcityLifeBacheca=10;
    public static boolean scrolledBacheca=false;
    public static FrameLayout buttonPiuBacheca;
    public static List<Bitmap> listImageComuneBacheca;
    public static List<String> urlimageComuneBacheca;
    public static Integer totalPreferiti = 0;
    public static List<Bitmap> listImagegofalonPref;

    public static int lenghtListRadar;
    public static AsyncTask<Void, Void, Void> asyncTaskAltriPoiRdar;
    public static int pageindexradar;



    //nuovi channale
    public static boolean nuovichannalbacheca;

    //
    public static String namecomuneTapped;
    public static long idcategorietapped;
    public static String distancemepoi;

    // Error Codes
    public static byte errorCode = 127;
    public static String segnalaEmail = "develop@tripfeatureapp.com";
    public static String errorFlag;

    public static String mapsmode = "&mode=driving";

    //prteferiti
    public static List<String> preferitifromfile;
    public static int tappedpref;

    public static LinkedHashMap<String, String> mHashMapDays = new LinkedHashMap <String, String>();
    public static LinkedHashMap<String, Boolean> mHashMapMonths = new LinkedHashMap<String, Boolean>();

    public static String specialHours;

    // OSM hours parsing UTIL
    public static String startTime = "00:00";
    public static String [] daysStr = new String[] {"Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato", "Domenica"};
    public static String[] monthsStr = new String[] {"Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"};


    public static boolean tripcitylifefatto;
    public static boolean canGo = false;
    //
    public static boolean endNews=false;
    public static boolean isComuneOrNot=true;
    //spostamento dei task async
    public static List<Bitmap> listImage;
    public static  List<String> listUrl ;
    public static List<String> listDistanceToMe ;
    public static  List<String> listNumFoto ;

    //urlimage azienda

    public static String urlimageazienda;

    //staticvarcontrolswitchstate
    public static  boolean controlIlMioComuneEsplora;
    public static  boolean controlAzienda;
    public static  boolean controlIlMioComuneActivity;

    public static boolean controlNuoveChannal;
    //new
    public static int tappedImgGonf;

    //url image poi senza immagini
    public static String urlPoiSenzaImagine;
    public static boolean imgPoiSenzaImg;
    public static boolean imgPoiSenzaImgNotFromDash;
    public static Bitmap imgPoiSenzaBack;

    // Più foto
    public static boolean piufoto;
    public static int tappedfoto=0;

    // Push / Parse variables.
    public static String pushDescription = "Resta sempre in contatto con i tuoi comuni preferiti, dal centro notifiche potrai gestire le comunicazioni in modo facile e veloce.";
    public static String pushTitle ="Comunicazione dallo Staff" ;

    public static List<PushCentroNotifiche> listCentroNotifiche = null;

    //var variabile nel app
    public static TextView nbPreferity;

    public static String appVersion = "1.6.5";

    public static String MDpath = "/Android/data/it.etinet.tripfeatureapp/Data/";
    //clearcheck
    public static FrameLayout clearCheck;

    public static String playLink = "https://play.google.com/store/apps/details?id=it.etinet.tripfeatureapp";

    public static String downloadPage = "http://www.tripfeatureapp.com/download/";

    public static String nomeComuneVerificaCopertura;

    //bacheca dettaglio news
    public static int tappedBacheca;

    //
    public static boolean comuneesplora;

    public static boolean networkAvailability = true;

    //map
    public static int pageindexmap;

    //gestione panoramio
    public static String fotoComuneUrl;

    //cerca e news
    public static boolean cercaeNews;
    public static String woeid_residenza;
    public static String  temperaturaFComune;
    public static String  codeTemperaturaComune;
    public static String txtMeteoDescriptionComune ;
    public static String txtMeteoGradiComune;

    public static boolean newPositionComune;
    public static boolean newPositionDettaglioNews;
    public static boolean newPositionCentroNotifiche;
    public static boolean newPositionSupport;

    //nuova gestione preferity
    public static ImageView editPref;
    public static boolean menoPeref;
    //condividitripcity life
    public static ImageView frmCondividi;
    public static boolean manomesso;
    public static boolean gotFile = true;
    public static boolean needUpdate = false;
    //city radar life mode
    public static boolean iamInCityRadar;
    public static boolean trackmode;
    public static double ragioCityRadar=0.2;
    public static boolean notMorePoiRadar;
    public static boolean moved=true;
    public  static boolean cerchiopiupoi;

    //utest
    public static final String APP_KEY_DEV1 = "f2d6a465de5c9409c7caeae53c11a358a8b86581";
    public static final String APP_KEY_DEV2="d3339bfc20d20e058287f47166ddc71512047733";
    //public static final String APP_KEY = "47ba3685dc231c6886eea6df30de7976e09a43aa";
    //public static final String APP_KEY = "f2d6a465de5c9409c7caeae53c11a358a8b86581";
    public static final String APP_KEY = "86e81d7cd213d2d9a9e883f13d9e6be29c4ede89";//LAST USE THIS!
    //poi segnalazione
    public static Double latitudePoiSegnalato;
    public static Double longitudePoiSegnalato;
    public static String namecomuneTappedSegnalato;
    public static String idcategorietappedSegnalato;
    public static String distancemepoiSegnalato;

    //Composizione segnalazione errore poi.
    public static String segnalaMappaNomePoi = "NoName";
    public static String segnalaMappaComuneDi = "NoComune";
    public static String deviceModel;


    //senza Gps modif
    public static boolean senzaGpsFinitoCatFragment;
    public  static boolean senzaGpsFinitoBachecaFragment;

    public static String segnalamappaIdPoi ;
    public static boolean noMoreParse = true;
    //control last poi
    public static boolean ImLastPoi;

    //informazione temperatura for comune froim dash
    public static String txtMeteoDescriptionFromDash;
    public static String txtMeteoGradiFromDash;
    public static Drawable imagemeteoFromDash;

    public static boolean scaricarePoiFromDash=false;

}
