package it.etinet.tripfeatureapp.Vars;



public class FunctionVar {

    public static String weatherTaskId(String comune,String[] comuneId){
        String[] s;
        for(int i=0;i<comuneId.length;i++){
            s = comuneId[i].split(",");
            if(s[0].equals(comune.replaceAll("%20"," "))){
                return s[1];
            }
        }
        return null;
    }
}