package it.etinet.tripfeatureapp.Model;


public class ObjectID {

    public String $oid;

    public String get$oid() {
        return $oid;
    }

    public void set$oid(String $oid) {
        this.$oid = $oid;
    }
}
