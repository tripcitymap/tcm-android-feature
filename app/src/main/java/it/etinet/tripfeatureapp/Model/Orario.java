package it.etinet.tripfeatureapp.Model;


public class Orario {

    private String [] chiusuraSettimanale;
    private EventTimeInfo [] chiusureStraordinarie;
    private EventTimeInfo  aperturaStandard;
    private EventTimeInfo [] aperturaStraordinarie;

    public String[] getChiusuraSettimanale() {
        return chiusuraSettimanale;
    }

    public void setChiusuraSettimanale(String[] chiusuraSettimanale) {
        this.chiusuraSettimanale = chiusuraSettimanale;
    }

    public EventTimeInfo[] getChiusureStraordinarie() {
        return chiusureStraordinarie;
    }

    public void setChiusureStraordinarie(EventTimeInfo[] chiusureStraordinarie) {
        this.chiusureStraordinarie = chiusureStraordinarie;
    }

    public EventTimeInfo getAperturaStandard() {
        return aperturaStandard;
    }

    public void setAperturaStandard(EventTimeInfo aperturaStandard) {
        this.aperturaStandard = aperturaStandard;
    }

    public EventTimeInfo[] getAperturaStraordinarie() {
        return aperturaStraordinarie;
    }

    public void setAperturaStraordinarie(EventTimeInfo[] aperturaStraordinarie) {
        this.aperturaStraordinarie = aperturaStraordinarie;
    }
}
