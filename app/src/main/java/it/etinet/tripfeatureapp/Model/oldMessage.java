package it.etinet.tripfeatureapp.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class oldMessage {

    private int id;
    private String creationDate;
    private String dateForClient;
    private String name;
    private String description;
    private String bigPictureUrl;
    private String midPictureUrl;
    private String icoPictureUrl;
    private int ownerId;

    @JsonProperty("Id")
    public int getId() {
        return id;
    }
    @JsonProperty("Id")
    public void setId(int id) {
        this.id = id;
    }
    @JsonProperty("OwnerId")
    public int getOwnerId() {
        return ownerId;
    }
    @JsonProperty("OwnerId")
    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }
    @JsonProperty("IcoPictureUrl")
    public String getIcoPictureUrl() {
        return icoPictureUrl;
    }
    @JsonProperty("IcoPictureUrl")
    public void setIcoPictureUrl(String icoPictureUrl) {
        this.icoPictureUrl = icoPictureUrl;
    }
    @JsonProperty("MidPictureUrl")
    public String getMidPictureUrl() {
        return midPictureUrl;
    }
    @JsonProperty("MidPictureUrl")
    public void setMidPictureUrl(String midPictureUrl) {
        this.midPictureUrl = midPictureUrl;
    }
    @JsonProperty("BigPictureUrl")
    public String getBigPictureUrl() {
        return bigPictureUrl;
    }
    @JsonProperty("BigPictureUrl")
    public void setBigPictureUrl(String bigPictureUrl) {
        this.bigPictureUrl = bigPictureUrl;
    }
    @JsonProperty("Description")
    public String getDescription() {
        return description;
    }
    @JsonProperty("Description")
    public void setDescription(String description) {
        this.description = description;
    }
    @JsonProperty("Name")
    public String getName() {
        return name;
    }
    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }
    @JsonProperty("DateForClient")
    public String getDateForClient() {
        return dateForClient;
    }
    @JsonProperty("DateForClient")
    public void setDateForClient(String dateForClient) {
        this.dateForClient = dateForClient;
    }
    @JsonProperty("CreationDate")
    public String getCreationDate() {
        return creationDate;
    }
    @JsonProperty("CreationDate")
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }
}


