package it.etinet.tripfeatureapp.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotoGallery {

    private String name;
    private Photo[] Photos;
    private TimeInfo timeInfo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TimeInfo getTimeInfo() {
        return timeInfo;
    }

    public void setTimeInfo(TimeInfo timeInfo) {
        this.timeInfo = timeInfo;
    }

    public Photo[] getPhotos() {
        return Photos;
    }

    public void setPhotos(Photo[] photos) {
        Photos = photos;
    }
}
