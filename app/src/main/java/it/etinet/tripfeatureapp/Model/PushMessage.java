package it.etinet.tripfeatureapp.Model;


public class PushMessage extends BusinnessEntityBase {

    private Long OwnerId;
    private Long AddressId;

    public void setOwnerId (Long OwnerId){this.OwnerId = OwnerId;}
    public Long getOwnerId (){return this.OwnerId;}

    public void setAddressId (Long AddressId){this.AddressId = AddressId;}
    public Long getAddressId (){return this.AddressId;}
}
