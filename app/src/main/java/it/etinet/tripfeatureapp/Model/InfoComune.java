package it.etinet.tripfeatureapp.Model;


public class InfoComune {

    private double Altitude;
    private int Population;
    private String seismicClass;
    private String [] Patrons;
    private String Holiday;
    private int istatCode;
    private double Surface;
    private String phonePrefix;
    private int [] Zips;
    private String WebSite;
    private GeoInfo center;

    public GeoInfo getCenter() {
        return center;
    }

    public void setCenter(GeoInfo center) {
        this.center = center;
    }

    public double getAltitude() {
        return Altitude;
    }

    public void setAltitude(double altitude) {
        Altitude = altitude;
    }


    public int[] getZips() {
        return Zips;
    }

    public void setZips(int[] zips) {
        Zips = zips;
    }

    public int getPopulation() {
        return Population;
    }

    public void setPopulation(int population) {
        Population = population;
    }

    public String getWebSite() {
        return WebSite;
    }

    public void setWebSite(String webSite) {
        WebSite = webSite;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public double getSurface() {
        return Surface;
    }

    public void setSurface(double surface) {
        Surface = surface;
    }

    public int getIstatCode() {
        return istatCode;
    }

    public void setIstatCode(int istatCode) {
        this.istatCode = istatCode;
    }

    public String getHoliday() {
        return Holiday;
    }

    public void setHoliday(String holiday) {
        Holiday = holiday;
    }

    public String[] getPatrons() {
        return Patrons;
    }

    public void setPatrons(String[] patrons) {
        Patrons = patrons;
    }

    public String getSeismicClass() {
        return seismicClass;
    }

    public void setSeismicClass(String seismicClass) {
        this.seismicClass = seismicClass;
    }
}
