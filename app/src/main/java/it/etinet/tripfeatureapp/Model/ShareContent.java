package it.etinet.tripfeatureapp.Model;


public class ShareContent {
    private String imageUrl;
    private String title;
    private String description;
    private static String downloadUrl;
    private String notes;
    private Address address;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public static String getDownloadUrl() {
        return downloadUrl;
    }

    public static void setDownloadUrl(String downloadUrl) {
        ShareContent.downloadUrl = downloadUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
