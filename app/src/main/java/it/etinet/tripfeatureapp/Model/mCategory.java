package it.etinet.tripfeatureapp.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

public class mCategory {

    private String id;
    private int totalPoi;
    private String [] childrenIds;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String[] getChildrenIds() {
        return childrenIds;
    }

    public void setChildrenIds(String[] childrenIds) {
        this.childrenIds = childrenIds;
    }

    public int getTotalPoi() {
        return totalPoi;
    }

    public void setTotalPoi(int totalPoi) {
        this.totalPoi = totalPoi;
    }
}
