package it.etinet.tripfeatureapp.Model;


public class LiteChannels {

    private long totalPoi;
    private String name;
    private String id;
    private Photo mainImage;
    private Photo iconImage;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getTotalPoi() {
        return totalPoi;
    }

    public void setTotalPoi(long totalPoi) {
        this.totalPoi = totalPoi;
    }

    public Photo getIconImage() {
        return iconImage;
    }

    public void setIconImage(Photo iconImage) {
        this.iconImage = iconImage;
    }

    public Photo getMainImage() {
        return mainImage;
    }

    public void setMainImage(Photo mainImage) {
        this.mainImage = mainImage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}