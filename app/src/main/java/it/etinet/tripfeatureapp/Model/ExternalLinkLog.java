package it.etinet.tripfeatureapp.Model;


public class ExternalLinkLog {
    private String Description_ar;
    private String Description_de;
    private String Description_en;
    private String Description_es;
    private String Description_fr;
    private String Description_it;
    private String Description_ja;
    private String Description_pt;
    private String Description_ru;
    private String Description_zh;
    private String IconURL;
    private String InternalID;
    private String Name_ar;
    private String Name_de;

    public String getName_ar() {
        return Name_ar;
    }

    public void setName_ar(String name_ar) {
        Name_ar = name_ar;
    }

    public String getName_de() {
        return Name_de;
    }

    public void setName_de(String name_de) {
        Name_de = name_de;
    }

    private String Name_en;
    private String Name_es;
    private String Name_fr;
    private String Name_it;
    private String Name_ja;
    private String Name_pt;
    private String Name_ru;
    private String Name_zh;
    private String Rapresentation;
    private TimeInfo TimeInfo;

    public String getDescription_ar() {
        return Description_ar;
    }

    public void setDescription_ar(String description_ar) {
        Description_ar = description_ar;
    }

    public String getDescription_es() {
        return Description_es;
    }

    public void setDescription_es(String description_es) {
        Description_es = description_es;
    }

    public String getName_en() {
        return Name_en;
    }

    public void setName_en(String name_en) {
        Name_en = name_en;
    }

    public String getName_pt() {
        return Name_pt;
    }

    public void setName_pt(String name_pt) {
        Name_pt = name_pt;
    }

    public TimeInfo getTimeInfo() {
        return TimeInfo;
    }

    public void setTimeInfo(TimeInfo timeInfo) {
        TimeInfo = timeInfo;
    }

    public String getRapresentation() {
        return Rapresentation;
    }

    public void setRapresentation(String rapresentation) {
        Rapresentation = rapresentation;
    }

    public String getName_zh() {
        return Name_zh;
    }

    public void setName_zh(String name_zh) {
        Name_zh = name_zh;
    }

    public String getName_ru() {
        return Name_ru;
    }

    public void setName_ru(String name_ru) {
        Name_ru = name_ru;
    }

    public String getName_it() {
        return Name_it;
    }

    public void setName_it(String name_it) {
        Name_it = name_it;
    }

    public String getName_fr() {
        return Name_fr;
    }

    public void setName_fr(String name_fr) {
        Name_fr = name_fr;
    }

    public String getName_es() {
        return Name_es;
    }

    public void setName_es(String name_es) {
        Name_es = name_es;
    }

    public String getInternalID() {
        return InternalID;
    }

    public void setInternalID(String internalID) {
        InternalID = internalID;
    }

    public String getDescription_pt() {
        return Description_pt;
    }

    public void setDescription_pt(String description_pt) {
        Description_pt = description_pt;
    }

    public String getDescription_it() {
        return Description_it;
    }

    public void setDescription_it(String description_it) {
        Description_it = description_it;
    }

    public String getDescription_fr() {
        return Description_fr;
    }

    public void setDescription_fr(String description_fr) {
        Description_fr = description_fr;
    }

    public String getDescription_ja() {
        return Description_ja;
    }

    public void setDescription_ja(String description_ja) {
        Description_ja = description_ja;
    }

    public String getName_ja() {
        return Name_ja;
    }

    public void setName_ja(String name_ja) {
        Name_ja = name_ja;
    }

    public String getDescription_ru() {
        return Description_ru;
    }

    public void setDescription_ru(String description_ru) {
        Description_ru = description_ru;
    }

    public String getIconURL() {
        return IconURL;
    }

    public void setIconURL(String iconURL) {
        IconURL = iconURL;
    }

    public String getDescription_zh() {
        return Description_zh;
    }

    public void setDescription_zh(String description_zh) {
        Description_zh = description_zh;
    }

    public String getDescription_en() {
        return Description_en;
    }

    public void setDescription_en(String description_en) {
        Description_en = description_en;
    }

    public String getDescription_de() {
        return Description_de;
    }

    public void setDescription_de(String description_de) {
        Description_de = description_de;
    }
}
