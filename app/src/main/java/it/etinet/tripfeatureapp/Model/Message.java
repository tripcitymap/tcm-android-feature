package it.etinet.tripfeatureapp.Model;


public class Message {

    private Address Address;

    private ObjectID Id;
    private String Id_Customer;
    private String Id_Poi;
    private long ExternalId;
    private boolean isResidenial;
    private boolean isTuristic;
    private int SendingRay;
    private String Title_de;
    private String Title_en;
    private String Title_fr;
    private String Title_it;
    private String message_de;
    private String message_en;
    private String message_fr;
    private String message_it;
    private String Status;

    public String getTitle_fr() {
        return Title_fr;
    }

    public void setTitle_fr(String title_fr) {
        Title_fr = title_fr;
    }

    public Address getAddress() {
        return Address;
    }

    public void setAddress(Address address) {
        Address = address;
    }

    public String getTitle_it() {
        return Title_it;
    }

    public void setTitle_it(String title_it) {
        Title_it = title_it;
    }

    public String getTitle_en() {
        return Title_en;
    }

    public void setTitle_en(String title_en) {
        Title_en = title_en;
    }

    public String getTitle_de() {
        return Title_de;
    }

    public void setTitle_de(String title_de) {
        Title_de = title_de;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public int getSendingRay() {
        return SendingRay;
    }

    public void setSendingRay(int sendingRay) {
        SendingRay = sendingRay;
    }

    public String getMessage_it() {
        return message_it;
    }

    public void setMessage_it(String message_it) {
        this.message_it = message_it;
    }

    public String getMessage_fr() {
        return message_fr;
    }

    public void setMessage_fr(String message_fr) {
        this.message_fr = message_fr;
    }

    public String getMessage_en() {
        return message_en;
    }

    public void setMessage_en(String message_en) {
        this.message_en = message_en;
    }

    public String getMessage_de() {
        return message_de;
    }

    public void setMessage_de(String message_de) {
        this.message_de = message_de;
    }

    public boolean isTuristic() {
        return isTuristic;
    }

    public void setTuristic(boolean isTuristic) {
        this.isTuristic = isTuristic;
    }

    public boolean isResidenial() {
        return isResidenial;
    }

    public void setResidenial(boolean isResidenial) {
        this.isResidenial = isResidenial;
    }

    public String getId_Poi() {
        return Id_Poi;
    }

    public void setId_Poi(String id_Poi) {
        Id_Poi = id_Poi;
    }

    public String getId_Customer() {
        return Id_Customer;
    }

    public void setId_Customer(String id_Customer) {
        Id_Customer = id_Customer;
    }

    public ObjectID getId() {
        return Id;
    }

    public void setId(ObjectID id) {
        Id = id;
    }

    public long getExternalId() {
        return ExternalId;
    }

    public void setExternalId(long externalId) {
        ExternalId = externalId;
    }


}
