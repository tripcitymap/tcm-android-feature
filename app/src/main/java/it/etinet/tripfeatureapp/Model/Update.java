package it.etinet.tripfeatureapp.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

public class Update {

    private String lastUpdateAssets;
    private String lastUpdateDb;

    public String getLastUpdateAssets() {
        return lastUpdateAssets;
    }

    public void setLastUpdateAssets(String lastUpdateAssets) {
        this.lastUpdateAssets = lastUpdateAssets;
    }

    public String getLastUpdateDb() {
        return lastUpdateDb;
    }

    public void setLastUpdateDb(String lastUpdateDb) {
        this.lastUpdateDb = lastUpdateDb;
    }

}
