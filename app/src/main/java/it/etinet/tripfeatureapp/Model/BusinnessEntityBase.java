package it.etinet.tripfeatureapp.Model;


public class BusinnessEntityBase {

    private String Name;
    private Long Id;
    private String IcoPictureUrl;
    private String MidPictureUrl;
    private String BigPictureUrl;
    private String Description;
    private Long OwnerId;
    private String BGPhoneUrl1;
    private String BGPhoneUrl2;
    private String BGPhoneUrl3;



    public void setName (String Name){this.Name = Name;}
    public String getName (){return this.Name;}

    public void setId (Long Id){this.Id = Id;}
    public Long getId (){return this.Id;}

    public void setIcoPictureUrl (String IcoPictureUrl){this.IcoPictureUrl = IcoPictureUrl;}
    public String getIcoPictureUrl (){return this.IcoPictureUrl;}

    public void setMidPictureUrl (String MidPictureUrl){this.MidPictureUrl = MidPictureUrl;}
    public String getMidPictureUrl (){return this.MidPictureUrl;}

    public void setBigPictureUrl  (String BigPictureUrl){this.BigPictureUrl = BigPictureUrl;}
    public String getBigPictureUrl  (){return this.BigPictureUrl;}

    public void setDescription (String Description){this.Description = Description;}
    public String getDescription (){return this.Description;}

    public void setOwnerId (Long OwnerId){this.OwnerId = OwnerId;}
    public Long getOwnerId (){return this.OwnerId;}

    public void setBGPhoneUrl1 (String BGPhoneUrl1){this.BGPhoneUrl1 = BGPhoneUrl1;}
    public String getBGPhoneUrl1 (){return this.BGPhoneUrl1;}

    public void setBGPhoneUrl2 (String BGPhoneUrl2){this.BGPhoneUrl2 = BGPhoneUrl2;}
    public String getBGPhoneUrl2 (){return this.BGPhoneUrl2;}

    public void setBGPhoneUrl3 (String BGPhoneUrl3){this.BGPhoneUrl3 = BGPhoneUrl3;}
    public String getBGPhoneUrl3 (){return this.BGPhoneUrl3;}

}
