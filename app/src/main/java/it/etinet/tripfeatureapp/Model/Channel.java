package it.etinet.tripfeatureapp.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties (ignoreUnknown = true)

public class Channel {

    private String totalPoi;
    private mCategory[] categories;
    private Address address;
    private String description;
    private Photo mainImage;
    private String name;
    private String website;
    private String phone;
    private PhotoGallery [] photoGallery;
    private int popularity;
    private GeoInfo geoInfo;
    private String id;
    private String cityId;
    private boolean isOfficial;
    private float distanceFromMe;
    private String distanceFromMeUOM;
    private Photo iconImage;
    private String type;
    private long woeid;

    public long getWoeid() {
        return woeid;
    }

    public void setWoeid(long woeid) {
        this.woeid = woeid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Photo getIconImage() {
        return iconImage;
    }

    public void setIconImage(Photo iconImage) {
        this.iconImage = iconImage;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public GeoInfo getGeoInfo() {
        return geoInfo;
    }

    public void setGeoInfo(GeoInfo geoInfo) {
        this.geoInfo = geoInfo;
    }

    public float getDistanceFromMe() {
        return distanceFromMe;
    }

    public void setDistanceFromMe(float distanceFromMe) {
        this.distanceFromMe = distanceFromMe;
    }

    public String getDistanceFromMeUOM() {
        return distanceFromMeUOM;
    }

    public void setDistanceFromMeUOM(String distanceFromMeUOM) {
        this.distanceFromMeUOM = distanceFromMeUOM;
    }

    public String getTotalPoi() {
        return totalPoi;
    }

    public void setTotalPoi(String totalPoi) {
        this.totalPoi = totalPoi;
    }

    public mCategory[] getCategories() {
        return categories;
    }

    public void setCategories(mCategory[] categories) {
        this.categories = categories;
    }

    public boolean isOfficial() {
        return isOfficial;
    }

    public void setOfficial(boolean isOfficial) {
        this.isOfficial = isOfficial;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public PhotoGallery[] getPhotoGallery() {
        return photoGallery;
    }

    public void setPhotoGallery(PhotoGallery[] photoGallery) {
        this.photoGallery = photoGallery;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Photo getMainImage() {
        return mainImage;
    }

    public void setMainImage(Photo mainImage) {
        this.mainImage = mainImage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
