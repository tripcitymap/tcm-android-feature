package it.etinet.tripfeatureapp.Model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

public class News {

    private String id;
    private String name;
    private String description;
    private TimeInfo timeInfo;
    private EventTimeInfo eventTimeInfo;
    private Photo [] photos;
    private int externalId;
    private String channelId;
    private String geoObjectId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getExternalId() {
        return externalId;
    }

    public void setExternalId(int externalId) {
        this.externalId = externalId;
    }

    public String getGeoObjectId() {
        return geoObjectId;
    }

    public void setGeoObjectId(String geoObjectId) {
        this.geoObjectId = geoObjectId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Photo[] getPhotos() {
        return photos;
    }

    public void setPhotos(Photo[] photos) {
        this.photos = photos;
    }

    public EventTimeInfo getEventTimeInfo() {
        return eventTimeInfo;
    }

    public void setEventTimeInfo(EventTimeInfo eventTimeInfo) {
        this.eventTimeInfo = eventTimeInfo;
    }

    public TimeInfo getTimeInfo() {
        return timeInfo;
    }

    public void setTimeInfo(TimeInfo timeInfo) {
        this.timeInfo = timeInfo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
