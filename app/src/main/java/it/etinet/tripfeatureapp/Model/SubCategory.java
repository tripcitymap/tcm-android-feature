package it.etinet.tripfeatureapp.Model;


public class SubCategory {

    private int totalPoi;
    private String name;
    private TimeInfo timeInfo;
    private String id_Father;
    private String internal_Id;
    private String color;

        public int getTotalPoi() {
            return totalPoi;
        }

        public void setTotalPoi(int totalPoi) {
            this.totalPoi = totalPoi;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getInternal_Id() {
            return internal_Id;
        }

        public void setInternal_Id(String internal_Id) {
            this.internal_Id = internal_Id;
        }

        public String getId_Father() {
            return id_Father;
        }

        public void setId_Father(String id_Father) {
            this.id_Father = id_Father;
        }

        public TimeInfo getTimeInfo() {
            return timeInfo;
        }

        public void setTimeInfo(TimeInfo timeInfo) {
            this.timeInfo = timeInfo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
}
