package it.etinet.tripfeatureapp.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties (ignoreUnknown = true)
public class GeoInfo {

    private Coordinates coordinates;
    private int type;
    private String boundingBox;
    private String coordinateReferenceSystem;
    private String extraMembers;

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public String getExtraMembers() {
        return extraMembers;
    }

    public void setExtraMembers(String extraMembers) {
        this.extraMembers = extraMembers;
    }

    public String getCoordinateReferenceSystem() {
        return coordinateReferenceSystem;
    }

    public void setCoordinateReferenceSystem(String coordinateReferenceSystem) {
        this.coordinateReferenceSystem = coordinateReferenceSystem;
    }

    public String getBoundingBox() {
        return boundingBox;
    }

    public void setBoundingBox(String boundingBox) {
        this.boundingBox = boundingBox;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
