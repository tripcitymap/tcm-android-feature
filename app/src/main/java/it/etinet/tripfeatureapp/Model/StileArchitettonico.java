package it.etinet.tripfeatureapp.Model;


public class StileArchitettonico {

    private String lang;
    private String description;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
