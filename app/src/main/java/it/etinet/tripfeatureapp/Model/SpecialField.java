package it.etinet.tripfeatureapp.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

/*@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXTERNAL_PROPERTY,
        property = "typology")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ObjectBrand.class, name = "object-brand"),
        @JsonSubTypes.Type(value = ObjectFuelPrice.class, name = "object-fuelprice") })
*/

public class SpecialField {

    private String name;
    private String typology;
    private String description;
    private int order;
    private int representation;
    private TimeInfo timeInfo;
    private String internalID;
    private Object value;

   // private Value [] value;

    /*@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
                  include = JsonTypeInfo.As.EXTERNAL_PROPERTY,
                  property = "typology")
    @JsonSubTypes(value={
            @JsonSubTypes.Type(value = ObjectBrand.class, name = "object-brand"),
            @JsonSubTypes.Type(value = ObjectFuelPrice.class, name = "object-fuelprice")
    })
    public void setValue (Value [] value){
        this.value = value;
    }*/

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getInternalID() {
        return internalID;
    }

    public void setInternalID(String internalID) {
        this.internalID = internalID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TimeInfo getTimeInfo() {
        return timeInfo;
    }

    public void setTimeInfo(TimeInfo timeInfo) {
        this.timeInfo = timeInfo;
    }

    public int getRepresentation() {
        return representation;
    }

    public void setRepresentation(int representation) {
        this.representation = representation;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypology() {
        return typology;
    }

    public void setTypology(String typology) {
        this.typology = typology;
    }

    //public Value [] getValue() {
    //    return value;
   // }
}