package it.etinet.tripfeatureapp.Model;


import android.graphics.drawable.Drawable;

public class Meteo {

    private String meteoDescription;
    private String meteoGradi;
    private Drawable meteoIcon;

    public String getMeteoDescription() {
        return meteoDescription;
    }

    public void setMeteoDescription(String meteoDescription) {
        this.meteoDescription = meteoDescription;
    }

    public Drawable getMeteoIcon() {
        return meteoIcon;
    }

    public void setMeteoIcon(Drawable meteoIcon) {
        this.meteoIcon = meteoIcon;
    }

    public String getMeteoGradi() {
        return meteoGradi;
    }

    public void setMeteoGradi(String meteoGradi) {
        this.meteoGradi = meteoGradi;
    }
}
