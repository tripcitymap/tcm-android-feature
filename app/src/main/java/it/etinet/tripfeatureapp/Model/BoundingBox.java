package it.etinet.tripfeatureapp.Model;

public class BoundingBox {

    GeoPoint pointSudWest;
    GeoPoint pointNordEst;

    public void setSudWestPoint( GeoPoint point){
        this.pointSudWest=point;
    }

    public GeoPoint getSudWestPoint(){
        return pointSudWest;
    }

    public void setNordEstPoint( GeoPoint point){
        this.pointNordEst=point;
    }

    public GeoPoint getNordEstPoint(){
        return pointNordEst;
    }


}
