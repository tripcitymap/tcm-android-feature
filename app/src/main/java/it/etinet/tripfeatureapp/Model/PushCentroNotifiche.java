package it.etinet.tripfeatureapp.Model;

public class PushCentroNotifiche {

    private String TitoloNotifica;
    private String QuantoTempoFa;
    private String DescrizioneNotifica;
    private String TipologiaNotifica;
    private String OwnerNotifica;

    private String IdNotifica;
    private String channelId;
    //Defines Attributes

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public void setTitoloNotifica (String TitoloNotifica){this.TitoloNotifica = TitoloNotifica;}
    public String getTitoloNotifica (){return this.TitoloNotifica;}

    public void setQuantoTempoFa (String QuantoTempoFa){this.QuantoTempoFa = QuantoTempoFa;}
    public String getQuantoTempoFa (){return this.QuantoTempoFa;}

    public void setDescrizioneNotifica (String DescrizioneNotifica){this.DescrizioneNotifica = DescrizioneNotifica;}
    public String getDescrizioneNotifica (){return this.DescrizioneNotifica;}

    public void setTipologiaNotifica (String TipologiaNotifica){this.TipologiaNotifica = TipologiaNotifica;}
    public String getTipologiaNotifica (){return this.TipologiaNotifica;}

    public void setIdNotifica (String IdNotifica){this.IdNotifica = IdNotifica;}
    public String getIdNotifica(){return this.IdNotifica;}

    public void setOwnerNotifica (String OwnerNotifica){this.OwnerNotifica = OwnerNotifica;}
    public String getOwnerNotifica (){return this.OwnerNotifica;}
}
