package it.etinet.tripfeatureapp.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Statistics {
    private int visitsCounter;

    public int getVisitsCounter() {
        return visitsCounter;
    }

    public void setVisitsCounter(int visitsCounter) {
        this.visitsCounter = visitsCounter;
    }
}
