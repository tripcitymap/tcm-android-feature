package it.etinet.tripfeatureapp.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

public class GeoObject {

    private String name;
    private String description;
    private String typology;
    private double popularity;
    private String email;
    private double rating;
    private boolean featured;
    private String publicURL;
    private String orientation;
    private double premiumPercentage;
    private String [] phones;
    private String [] tags;
    private PhotoGallery [] photoGallery;
    private Photo mainImage;
    private Address address;
    private TimeInfo timeInfo;
    private EventTimeInfo eventTimeInfo;
    private Statistics statistics;
    private ExternalLink [] externalLinks;
    private GeoInfo geoInfo;
    private String [] innerGeoObject;
    private String origin;
    private int NumberOfPhotos;
    private float distanceFromMe;
    private String distanceFromMeUOM;
    private String id;
    private String categoryId;
    private String [] channelsId;
    private String [] servicesId;
    private Representation representation;
    private SpecialField [] specialField;
    private String inappFrom;

    public String getInappFrom() {
        return inappFrom;
    }

    public void setInappFrom(String inappFrom) {
        this.inappFrom = inappFrom;
    }

    public void setChannelsId(String[] channelsId) {
        this.channelsId = channelsId;
    }

    public GeoInfo getGeoInfo() {
        return geoInfo;
    }

    public void setGeoInfo(GeoInfo geoInfo) {
        this.geoInfo = geoInfo;
    }

    public float getDistanceFromMe() {
        return distanceFromMe;
    }

    public void setDistanceFromMe(float distanceFromMe) {
        this.distanceFromMe = distanceFromMe;
    }

    public String getDistanceFromMeUOM() {
        return distanceFromMeUOM;
    }

    public void setDistanceFromMeUOM(String distanceFromMeUOM) {
        this.distanceFromMeUOM = distanceFromMeUOM;
    }

    public int getNumberOfPhotos() {
        return NumberOfPhotos;
    }

    public void setNumberOfPhotos(int numberOfPhotos) {
        NumberOfPhotos = numberOfPhotos;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    public Representation getRepresentation() {
        return representation;
    }

    public void setRepresentation(Representation representation) {
        this.representation = representation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypology() {
        return typology;
    }

    public void setTypology(String typology) {
        this.typology = typology;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public String getPublicURL() {
        return publicURL;
    }

    public void setPublicURL(String publicURL) {
        this.publicURL = publicURL;
    }

    public double getPremiumPercentage() {
        return premiumPercentage;
    }

    public void setPremiumPercentage(double premiumPercentage) {
        this.premiumPercentage = premiumPercentage;
    }

    public String[] getPhones() {
        return phones;
    }

    public void setPhones(String[] phones) {
        this.phones = phones;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public PhotoGallery[] getPhotoGallery() {
        return photoGallery;
    }

    public void setPhotoGallery(PhotoGallery[] photoGallery) {
        this.photoGallery = photoGallery;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public EventTimeInfo getEventTimeInfo() {
        return eventTimeInfo;
    }

    public void setEventTimeInfo(EventTimeInfo eventTimeInfo) {
        this.eventTimeInfo = eventTimeInfo;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SpecialField[] getSpecialField() {
        return specialField;
    }

    public void setSpecialField(SpecialField[] specialField) {
        this.specialField = specialField;
    }

    public String[] getServicesId() {
        return servicesId;
    }

    public void setServicesId(String[] servicesId) {
        this.servicesId = servicesId;
    }

    public String[] getChannelsId() {
        return channelsId;
    }

    public void setChannelId(String[] channelsId) {
        this.channelsId = channelsId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String[] getInnerGeoObject() {
        return innerGeoObject;
    }

    public void setInnerGeoObject(String[] innerGeoObject) {
        this.innerGeoObject = innerGeoObject;
    }


    public ExternalLink[] getExternalLinks() {
        return externalLinks;
    }

    public void setExternalLinks(ExternalLink[] externalLinks) {
        this.externalLinks = externalLinks;
    }

    public Statistics getStatistics() {
        return statistics;
    }

    public void setStatistics(Statistics statistics) {
        this.statistics = statistics;
    }

    public TimeInfo getTimeInfo() {
        return timeInfo;
    }

    public void setTimeInfo(TimeInfo timeInfo) {
        this.timeInfo = timeInfo;
    }

    public Photo getMainImage() {
        return mainImage;
    }

    public void setMainImage(Photo mainImage) {
        this.mainImage = mainImage;
    }
}