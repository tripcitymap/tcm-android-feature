package it.etinet.tripfeatureapp.Model;

public class SrvDescriptionElement {

    int sid;
    String descr;

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }
}
