package it.etinet.tripfeatureapp.Model;


public class DBObject {

    private String name;
    private String flag;
    private String id_mongo;
    private String gonfalone;
    private String type;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGonfalone() {
        return gonfalone;
    }

    public void setGonfalone(String gonfalone) {
        this.gonfalone = gonfalone;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getId_mongo() {
        return id_mongo;
    }

    public void setId_mongo(String id_mongo) {
        this.id_mongo = id_mongo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
