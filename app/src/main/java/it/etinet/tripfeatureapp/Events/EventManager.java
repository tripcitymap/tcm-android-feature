package it.etinet.tripfeatureapp.Events;

import java.util.List;

import de.greenrobot.event.EventBus;
import it.etinet.tripfeatureapp.Core.Poison;
import it.etinet.tripfeatureapp.Core.SingletonRestClient;
import it.etinet.tripfeatureapp.Events.Model.AskedEvent;
import it.etinet.tripfeatureapp.Events.Model.EventManagerError;
import it.etinet.tripfeatureapp.Events.Model.KillEvent;
import it.etinet.tripfeatureapp.Events.Model.ListOfChannels;
import it.etinet.tripfeatureapp.Events.Model.ListOfGeoObjects;
import it.etinet.tripfeatureapp.Events.Model.ListOfNews;
import it.etinet.tripfeatureapp.Events.Model.SingleChannel;
import it.etinet.tripfeatureapp.Model.Channel;
import it.etinet.tripfeatureapp.Model.GeoObject;
import it.etinet.tripfeatureapp.Model.News;
import it.etinet.tripfeatureapp.ModelSpecial.EventHandler;
import it.etinet.tripfeatureapp.Vars.StaticVar;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class EventManager {

    private String woeid;
    private String channelId;
    // React on event received - Objects asked for an event
    public void onEvent(AskedEvent event) {
        String eventType = event.getEventType();
        if("getLastNaturePois".equals(eventType)){
            getLastNaturePois();
        }else if("getLastArtPois".equals(eventType)){
            getLastArtPois();
        }else if("getLastPremiumPois".equals(eventType)){
            getLastPremiumPois();
        }else if("getLastChannels".equals(eventType)){
            getLastChannels();
        }else if("getChannelById".equals(eventType)){
            this.channelId = event.getChannelId();
            getChannelById();
        }else if("getInformationCityPosition".equals(eventType)){
            getInformationCityPosition();
        }else if("getListOfChannelNews".equals(eventType)){
            getListOfChannelNews();
        }
    }

    public void onEvent(KillEvent event){

    }

    //CityPositionInformations
    private void getInformationCityPosition (){
        final long start = System.currentTimeMillis();
        final EventHandler eventHandler = new EventHandler();
        eventHandler.setCategory("getChannelByPosition");
        eventHandler.setEventType((byte) 1);
        Poison restClient = SingletonRestClient.INSTANCE.getRestClient();
        String uPosition = StaticVar.latitude + "," + StaticVar.longitude;
        restClient.getChannelByPosition(uPosition, "html", new Callback<Channel>() {
            @Override
            public void success(Channel channel, Response response) {
                SingleChannel singleChannel = new SingleChannel();
                singleChannel.setChannel(channel);
                singleChannel.setRequestType("getInformationCityPosition");
                singleChannel.setStartTime(start);

                EventBus.getDefault().post(singleChannel);
            }

            @Override
            public void failure(RetrofitError error) {
                reportError(error, "getInformationCityPositionError", start, false);
            }
        });
    }

    // "Answer" at ListOfGeoObjects EVENT
    private void getLastNaturePois () {
        Poison restClient = SingletonRestClient.INSTANCE.getRestClient();
        final long start = System.currentTimeMillis();
        final EventHandler eventHandler = new EventHandler();
        eventHandler.setCategory("getLastPois");
        eventHandler.setEventType((byte)1);
        restClient.getLastPois(0, 2, "nature", "html", new Callback<List<GeoObject>>() {
            @Override
            public void success(List<GeoObject> geoObjects, Response response) {
                ListOfGeoObjects listOfGeoObjects = new ListOfGeoObjects();
                listOfGeoObjects.setGeoObjectList(geoObjects);
                listOfGeoObjects.setRequestType("getLastNaturePois");
                listOfGeoObjects.setStartTime(start);
                //Posting an event called ListOfGeoObjects
                EventBus.getDefault().post(listOfGeoObjects);
            }

            @Override
            public void failure(RetrofitError error) {
                reportError(error, "getLastNaturePoisError", start, false);
            }
        });
    }
    private void getLastArtPois () {
        final long start = System.currentTimeMillis();
        final EventHandler eventHandler = new EventHandler();
        eventHandler.setCategory("getLastPois");
        eventHandler.setEventType((byte)1);
        Poison restClient = SingletonRestClient.INSTANCE.getRestClient();
        restClient.getLastPois(0,4,"art","html",new Callback<List<GeoObject>>() {//art
            @Override
            public void success(List<GeoObject> geoObjects, Response response) {
                ListOfGeoObjects listOfGeoObjects = new ListOfGeoObjects();
                listOfGeoObjects.setGeoObjectList(geoObjects);
                listOfGeoObjects.setRequestType("getLastArtPois");
                listOfGeoObjects.setStartTime(start);
                //Posting an event called ListOfGeoObjects
                EventBus.getDefault().post(listOfGeoObjects);
            }
            @Override
            public void failure(RetrofitError error) {
                reportError(error,"getLastArtPoisError",start, false);
            }
        });
    }
    private void getLastPremiumPois () {
        final long start = System.currentTimeMillis();
        final EventHandler eventHandler = new EventHandler();
        eventHandler.setCategory("getLastPois");
        eventHandler.setEventType((byte)1);
        Poison restClient = SingletonRestClient.INSTANCE.getRestClient();
        restClient.getLastPois(0, 4, "premium", "html", new Callback<List<GeoObject>>() {
            @Override
            public void success(List<GeoObject> geoObjects, Response response) {
                ListOfGeoObjects listOfGeoObjects = new ListOfGeoObjects();
                listOfGeoObjects.setGeoObjectList(geoObjects);
                listOfGeoObjects.setRequestType("getLastPremiumPois");
                listOfGeoObjects.setStartTime(start);
                //Posting an event called ListOfGeoObjects
                EventBus.getDefault().post(listOfGeoObjects);
            }

            @Override
            public void failure(RetrofitError error) {
                reportError(error, "getLastPremiumPoisError", start, false);
            }
        });
    }

    // "Answer" at ListOfChannels EVENT
    private void getLastChannels() {
        final long start = System.currentTimeMillis();
        final EventHandler eventHandler = new EventHandler();
        eventHandler.setCategory("getLastChannels");
        eventHandler.setEventType((byte) 1);
        Poison restClient = SingletonRestClient.INSTANCE.getRestClient();
        restClient.getLastChannels(2, "html", new Callback<List<Channel>>() {
            @Override
            public void success(List<Channel> channels, Response response) {
                ListOfChannels listOfChannels = new ListOfChannels();
                listOfChannels.setChannelList(channels);
                listOfChannels.setStartTime(start);
                listOfChannels.setRequestType("getLastChannels");
                EventBus.getDefault().post(listOfChannels);
            }

            @Override
            public void failure(RetrofitError error) {
                reportError(error, "getLastChannelsError", start, false);
            }
        });
    }

    // "Answer" at ChannelById EVENT
    private void getChannelById () {
        final long start = System.currentTimeMillis();
        final EventHandler eventHandler = new EventHandler();
        eventHandler.setCategory("getChannelById");
        final String requestedChannelId = this.channelId;
        eventHandler.setEventType((byte) 1);
        Poison restClient = SingletonRestClient.INSTANCE.getRestClient();
        restClient.getChannelById(this.channelId, "html", new Callback<Channel>() {
            @Override
            public void success(Channel channel, Response response) {
                if (channel != null) {
                    SingleChannel singleChannel = new SingleChannel();
                    singleChannel.setChannel(channel);
                    singleChannel.setRequestType("getChannelById");
                    singleChannel.setStartTime(start);
                    singleChannel.setRequestedChannelId(requestedChannelId);
                    //Posting an event called ListOfGeoObjects
                    EventBus.getDefault().post(singleChannel);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                reportError(error, "getChannelByIdError", start, true);
            }
        });
    }

    // "Answer" at News By Channel Id EVENT
    private void getListOfChannelNews () {
        Poison restService = SingletonRestClient.INSTANCE.getRestClient();
        final long start = System.currentTimeMillis();
        final EventHandler eventHandler = new EventHandler();
        eventHandler.setCategory("getNewsByChannelId");
        eventHandler.setEventType((byte)1);

        final String requestedChannelId = this.channelId;

        restService.getNewsByChannelId(requestedChannelId, "html", new Callback<List<News>>() {
            @Override
            public void success(List<News> newses, Response response) {
                ListOfNews listOfNews = new ListOfNews();
                listOfNews.setListNews(newses);
                listOfNews.setRequestType("getNewsByChannelId");
                listOfNews.setStartTime(start);
                listOfNews.setRequestedChannelId(requestedChannelId);

                EventBus.getDefault().post(listOfNews);

            }

            @Override
            public void failure(RetrofitError error) {
                reportError(error, "getNewsByChannelIdError", start, true);
            }
        });

    }


    // "Answer" at GeneralError EVENT
    private void reportError (RetrofitError error,String errorFrom, long startTime, boolean fromChannel) {
        //Posting on bus the EventManagerError (Every Object who is listening to this error must check his type
        EventManagerError eme = new EventManagerError();
        eme.setError(error);
        eme.setErrorType(errorFrom);
        eme.setStartTime(startTime);

        if(fromChannel){
            eme.setRequestedChannelId(this.channelId);
        }
        EventBus.getDefault().post(eme);
    }

}