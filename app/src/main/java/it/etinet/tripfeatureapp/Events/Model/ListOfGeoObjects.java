package it.etinet.tripfeatureapp.Events.Model;


import java.util.List;

import it.etinet.tripfeatureapp.Model.GeoObject;

// Event Type
public class ListOfGeoObjects extends RetroFather {

    private List<GeoObject> geoObjectList;

    public List<GeoObject> getGeoObjectList() {
        return geoObjectList;
    }

    public void setGeoObjectList(List<GeoObject> geoObjectList) {
        this.geoObjectList = geoObjectList;
    }

}
