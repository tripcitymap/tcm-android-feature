package it.etinet.tripfeatureapp.Events.Model;

import it.etinet.tripfeatureapp.Model.Channel;

//Event Type
public class SingleChannel extends RetroFather {

    private Channel channel;
    private String requestedChannelId;

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public String getRequestedChannelId() {
        return requestedChannelId;
    }

    public void setRequestedChannelId(String requestedChannelId) {
        this.requestedChannelId = requestedChannelId;
    }
}
