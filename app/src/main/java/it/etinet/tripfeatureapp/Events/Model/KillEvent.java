package it.etinet.tripfeatureapp.Events.Model;


public class KillEvent {

    private String whoToKill;

    public String getWhoToKill() {
        return whoToKill;
    }

    public void setWhoToKill(String whoToKill) {
        this.whoToKill = whoToKill;
    }
}
