package it.etinet.tripfeatureapp.Events.Model;

import java.util.List;

import it.etinet.tripfeatureapp.Model.Channel;

//Event Type
public class ListOfChannels extends RetroFather {

    private List<Channel> channelList;

    public List<Channel> getChannelList() {
        return channelList;
    }

    public void setChannelList(List<Channel> channelList) {
        this.channelList = channelList;
    }

}
