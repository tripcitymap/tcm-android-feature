package it.etinet.tripfeatureapp.Events.Model;


import retrofit.RetrofitError;

public class EventManagerError extends RetroFather {

    private RetrofitError error;
    private String requestedChannelId;

    public RetrofitError getError() {
        return error;
    }

    public void setError(RetrofitError error) {
        this.error = error;
    }

    public String getRequestedChannelId() {
        return requestedChannelId;
    }

    public void setRequestedChannelId(String requestedChannelId) {
        this.requestedChannelId = requestedChannelId;
    }
}
