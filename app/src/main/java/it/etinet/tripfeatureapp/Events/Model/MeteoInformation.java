package it.etinet.tripfeatureapp.Events.Model;


import android.graphics.drawable.Drawable;

public class MeteoInformation {

    private String meteoDescription;
    private String meteoDegrees;
    private Drawable meteoIcon;

    public String getMeteoDescription() {
        return meteoDescription;
    }

    public void setMeteoDescription(String meteoDescription) {
        this.meteoDescription = meteoDescription;
    }

    public Drawable getMeteoIcon() {
        return meteoIcon;
    }

    public void setMeteoIcon(Drawable meteoIcon) {
        this.meteoIcon = meteoIcon;
    }

    public String getMeteoDegrees() {
        return meteoDegrees;
    }

    public void setMeteoDegrees(String meteoDegrees) {
        this.meteoDegrees = meteoDegrees;
    }
}
