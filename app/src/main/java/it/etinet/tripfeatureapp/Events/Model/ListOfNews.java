package it.etinet.tripfeatureapp.Events.Model;


import java.util.List;

import it.etinet.tripfeatureapp.Model.News;

public class ListOfNews extends RetroFather {

    private List<News> listNews;
    private String requestedChannelId;

    public List<News> getListNews() {
        return listNews;
    }

    public void setListNews(List<News> listNews) {
        this.listNews = listNews;
    }

    public String getRequestedChannelId() {
        return requestedChannelId;
    }

    public void setRequestedChannelId(String requestedChannelId) {
        this.requestedChannelId = requestedChannelId;
    }
}
