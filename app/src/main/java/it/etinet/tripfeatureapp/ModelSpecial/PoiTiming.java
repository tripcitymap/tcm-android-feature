package it.etinet.tripfeatureapp.ModelSpecial;


public class PoiTiming {

    private String mOpenTime;
    private String mCloseTime;

    private String aOpenTime;
    private String aCloseTime;

    public String getmOpenTime() {
        return mOpenTime;
    }

    public void setmOpenTime(String mOpenTime) {
        this.mOpenTime = mOpenTime;
    }

    public String getaCloseTime() {
        return aCloseTime;
    }

    public void setaCloseTime(String aCloseTime) {
        this.aCloseTime = aCloseTime;
    }

    public String getaOpenTime() {
        return aOpenTime;
    }

    public void setaOpenTime(String aOpenTime) {
        this.aOpenTime = aOpenTime;
    }

    public String getmCloseTime() {
        return mCloseTime;
    }

    public void setmCloseTime(String mCloseTime) {
        this.mCloseTime = mCloseTime;
    }
}
