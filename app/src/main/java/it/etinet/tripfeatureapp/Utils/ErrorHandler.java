package it.etinet.tripfeatureapp.Utils;


import retrofit.RetrofitError;
import retrofit.client.Response;

// Helper class that returns "Friendly" text from an RetrofitError object.
public class ErrorHandler {

    private RetrofitError error;
    private String errorSource;
    private String askedRequest;

    public ErrorHandler (RetrofitError error, String errorSource, String askedRequest) {
        this.error = error;
        this.errorSource = errorSource;
        this.askedRequest = askedRequest;
    }

    public String handleError () {
        String errorDescription;
        if(this.error != null){
            String errorUrl = error.getUrl();
            if(!errorGetKind(error).isEmpty()){
                //We got error kind.
                errorDescription = "Si è verificato un errore nella request " + this.askedRequest + ". L'errore provviene da " + errorSource + ". Ulteriori informazioni: " + errorGetKind(error);
                if(!errorResponseParser(error).isEmpty()){
                    //We got more error details.
                    errorDescription = errorDescription + " - " + errorResponseParser(error);
                }
            }else {
                errorDescription = "Si è verificato un errore nella request " + this.askedRequest + ". L'errore provviene da " + errorSource + ", l'url è : " + errorUrl;
                if(!errorResponseParser(error).isEmpty()){
                    //We got more error details.
                    errorDescription = errorDescription + " Ulteriori informazioni - " + errorResponseParser(error);
                }
            }
        }else {
            errorDescription = "Si è verificato un errore nella request " + this.askedRequest + ". L'errore provviene da " + errorSource + ". Questo è tutto quello che so.";
        }
        return errorDescription;
    }

    /**
     * What kind of error happened
     * @return empty if could not understand the error.
     * */
    private String errorGetKind (RetrofitError error) {
        String errorKid = "";
        if(error != null){
            if(error.getKind() != null){
                if(error.getKind() == RetrofitError.Kind.CONVERSION){
                    errorKid = "Retrofit CONVERSION Error";
                }else if(error.getKind() == RetrofitError.Kind.HTTP){
                    errorKid = "Retrofit HTTP Error";
                }else if(error.getKind() == RetrofitError.Kind.NETWORK){
                    errorKid = "Retrofit NETWORK Error";
                }else if(error.getKind() == RetrofitError.Kind.UNEXPECTED){
                    errorKid = "Retrofit UNEXPECTED Error";
                }else {
                    errorKid = "Retrofit Error not found";
                }
            }
        }
        return errorKid;
    }

    /**
     * Handle retrofit status for more informations
     * @return empty string if there are no available informatio
     * */
    private String errorResponseParser (RetrofitError error) {
        String errorResponse = "";
        if(error != null){
            if(error.getResponse() != null){
                Response response = error.getResponse();
                if(response.getReason() != null){
                    errorResponse = "Error Reason is: " + response.getReason();
                }else if(response.getStatus() != 0){
                    if(errorResponse.isEmpty()){
                        errorResponse = "Error statusCode is: " + response.getStatus();
                    }else {
                        errorResponse = errorResponse + " and the status code is: " + response.getStatus();
                    }
                }else if(response.getBody() != null){
                    if(errorResponse.isEmpty()){
                        errorResponse = "Error body is: " + response.getBody();
                    }else {
                        errorResponse = " and error body is: " + response.getBody();
                    }
                }
            }
        }
        return errorResponse;
    }
}