package it.etinet.tripfeatureapp.Utils;

import android.location.Location;

public class DistanzaGeodetica {

    public double disgeod (double latA, double lonA,double latB, double lonB)
    {
        Location mylocation = new Location("");
        Location dest_location = new Location("");

        mylocation.setLatitude(latA);
        mylocation.setLongitude(lonA);

        dest_location.setLatitude(latB);
        dest_location.setLongitude(lonB);

        return (double) mylocation.distanceTo(dest_location);
    }
}
