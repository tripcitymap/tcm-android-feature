package it.etinet.tripfeatureapp.Utils;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import it.etinet.tripfeatureapp.ModelSpecial.PoiTiming;
import it.etinet.tripfeatureapp.Vars.StaticVar;

public class OpeningHoursUtil {

    public void getOpenDays (OpeningHoursParser.OpeningHours openingHours){
        PoiTiming mPoiTiming = new PoiTiming();
        boolean aOt = false;
        if(openingHours != null) { // another check that the parser has done his job
            if(openingHours.getRules().size() > 1){
                // Means i got more then one rule
                for (int i = 0; i < openingHours.getRules().size(); i++) {
                    // Iterate all the rules
                    aOt = false;
                    OpeningHoursParser.OpeningHoursRule mRule = openingHours.getRules().get(i);
                    OpeningHoursParser.BasicOpeningHourRule mBasicRule = (OpeningHoursParser.BasicOpeningHourRule) mRule;

                    if (mBasicRule.getStartTime() != 0) {
                        int[] startTimes = mBasicRule.getStartTimes();
                        if (startTimes.length > 1) {
                            mPoiTiming.setmOpenTime(getOpensHours(mBasicRule.getStartTimes()[0]));
                            mPoiTiming.setaOpenTime(getOpensHours(mBasicRule.getStartTimes()[1]));
                            aOt = true;
                        } else {
                            mPoiTiming.setmOpenTime(getOpensHours(mBasicRule.getStartTime()));
                        }
                    }
                    if (mBasicRule.getEndTime() != 0) {
                        int[] endTimes = mBasicRule.getEndTimes();
                        if (endTimes.length > 1) {
                            mPoiTiming.setmCloseTime(getOpensHours(mBasicRule.getEndTimes()[0]));
                            mPoiTiming.setaCloseTime(getOpensHours(mBasicRule.getEndTimes()[1]));
                            aOt = true;
                        } else {
                            mPoiTiming.setmCloseTime(getOpensHours(mBasicRule.getEndTime()));
                        }
                    }

                    String mRuleString = mBasicRule.toRuleString();
                    if (!mRuleString.contains("off")){
                        if(i == 0) {
                            for (int k = 0; k < mBasicRule.getDays().length; k++) {
                                if (!mBasicRule.getDays()[k]) {
                                    add(StaticVar.mHashMapDays, k, StaticVar.daysStr[k], String.valueOf((mBasicRule.getDays()[k])) + "|");// Create Days Lhashmap
                                } else {
                                    if (aOt) {
                                        add(StaticVar.mHashMapDays, k, StaticVar.daysStr[k], String.valueOf((mBasicRule.getDays()[k])) + "|" + mPoiTiming.getmOpenTime() + "," + mPoiTiming.getmCloseTime() + "-" + mPoiTiming.getaOpenTime() + "," + mPoiTiming.getaCloseTime());// Create Days Lhashmap
                                    } else {
                                        add(StaticVar.mHashMapDays, k, StaticVar.daysStr[k], String.valueOf((mBasicRule.getDays()[k])) + "|" + mPoiTiming.getmOpenTime() + "," + mPoiTiming.getmCloseTime() + "-");// Create Days Lhashmap
                                    }
                                }
                            }
                        }else {
                            // TODO SECOND RULE ETC! NEED REPLACE THE 1ST RULE
                            int [] newRuleIndexs = new int[mBasicRule.getDays().length];
                            for (int j = 0; j < mBasicRule.getDays().length ; j++) {
                                if (mBasicRule.getDays()[j])
                                    newRuleIndexs[j] = 1;
                            }
                            //I got indexes where there are new rules to add.
                                for (int j = 0; j < newRuleIndexs.length; j++) {
                                    if (newRuleIndexs[j] == 1) {
                                        //im at index that i must change |J|
                                            if(aOt){
                                                add(StaticVar.mHashMapDays, j, StaticVar.daysStr[j], String.valueOf((mBasicRule.getDays()[j])) + "|" + mPoiTiming.getmOpenTime() + "," + mPoiTiming.getmCloseTime()+"-"+mPoiTiming.getaOpenTime()+","+mPoiTiming.getaCloseTime());// Create Days Lhashmap
                                            }else{
                                                add(StaticVar.mHashMapDays, j, StaticVar.daysStr[j], String.valueOf((mBasicRule.getDays()[j])) + "|" + mPoiTiming.getmOpenTime() + "," + mPoiTiming.getmCloseTime()+"-");// Create Days Lhashmap
                                            }
                                    }
                                }
                        }
                    }else {
                        //TODO RULE CONTAINS OFF
                        int [] newRuleIndexs = new int[mBasicRule.getDays().length];
                        for (int j = 0; j < mBasicRule.getDays().length ; j++) {
                            if (mBasicRule.getDays()[j])
                                newRuleIndexs[j] = 1;
                        }
                        for (int j = 0; j < newRuleIndexs.length; j++) {
                            if (newRuleIndexs[j] == 1) {
                                //im at index that i must change |J|
                                add(StaticVar.mHashMapDays, j, StaticVar.daysStr[j],"false|");
                            }
                        }
                    }

                    for (int j = 0; j < mBasicRule.getMonths().length; j++) {
                        add(StaticVar.mHashMapMonths,j,StaticVar.monthsStr[j],mBasicRule.getMonths()[j]);// Create Months Lhashmap
                    }

                }
            }else{ // Get the only 1 rule we've got
                OpeningHoursParser.OpeningHoursRule mRule = openingHours.getRules().get(0);
                OpeningHoursParser.BasicOpeningHourRule mBasicRule = (OpeningHoursParser.BasicOpeningHourRule) mRule;

                if("24/7".equals(mBasicRule.toString())){
                    int [] startTimes = mBasicRule.getStartTimes();
                    if(startTimes.length > 1){
                        mPoiTiming.setmOpenTime(getOpensHours(mBasicRule.getStartTimes()[0]));
                        mPoiTiming.setaOpenTime(getOpensHours(mBasicRule.getStartTimes()[1]));
                        aOt = true;
                    }else {
                        mPoiTiming.setmOpenTime(getOpensHours(mBasicRule.getStartTime()));
                    }
                }else if(mBasicRule.getStartTime() != 0 ){
                    int [] startTimes = mBasicRule.getStartTimes();
                    if(startTimes.length > 1){
                        mPoiTiming.setmOpenTime(getOpensHours(mBasicRule.getStartTimes()[0]));
                        mPoiTiming.setaOpenTime(getOpensHours(mBasicRule.getStartTimes()[1]));
                        aOt = true;
                    }else {
                        mPoiTiming.setmOpenTime(getOpensHours(mBasicRule.getStartTime()));
                    }
                }

                if(mBasicRule.getEndTime() != 0){
                    int [] endTimes = mBasicRule.getEndTimes();
                    if(endTimes.length > 1){
                        mPoiTiming.setmCloseTime(getOpensHours(mBasicRule.getEndTimes()[0]));
                        mPoiTiming.setaCloseTime(getOpensHours(mBasicRule.getEndTimes()[1]));
                        aOt = true;
                    }else {
                        mPoiTiming.setmCloseTime(getOpensHours(mBasicRule.getEndTime()));
                    }
                }

                for (int i = 0; i < mBasicRule.getDays().length; i++) {
                    if (!mBasicRule.getDays()[i]) {
                        add(StaticVar.mHashMapDays, i, StaticVar.daysStr[i], String.valueOf((mBasicRule.getDays()[i])) + "|");// Create Days Lhashmap
                    }else {
                        if(aOt){
                            add(StaticVar.mHashMapDays, i, StaticVar.daysStr[i], String.valueOf((mBasicRule.getDays()[i])) + "|" + mPoiTiming.getmOpenTime() + "," + mPoiTiming.getmCloseTime()+"-"+mPoiTiming.getaOpenTime()+","+mPoiTiming.getaCloseTime());// Create Days Lhashmap
                        }else{
                            add(StaticVar.mHashMapDays, i, StaticVar.daysStr[i], String.valueOf((mBasicRule.getDays()[i])) + "|" + mPoiTiming.getmOpenTime() + "," + mPoiTiming.getmCloseTime()+"-");// Create Days Lhashmap
                        }
                    }
                }
                for (int i = 0; i < mBasicRule.getMonths().length; i++) {
                    add(StaticVar.mHashMapMonths,i,StaticVar.monthsStr[i],mBasicRule.getMonths()[i]);// Create Months Lhashmap
                }
            }
        }else {
            //TODO Error parsing the String ive got in input.
        }
    }

    // Get H + M format Time
    public static String getOpensHours (int minutes) {
        int h = minutes / 60 + Integer.valueOf(StaticVar.startTime.substring(0,1));
        int m = minutes % 60 + Integer.valueOf(StaticVar.startTime.substring(3,4));
        return h+":"+m;
    }

    // ADD at correct index.
    public static <K, V> void add(LinkedHashMap<K, V> map, int index, K key, V value) {
        //assert (map != null);
        //assert !map.containsKey(key);
        //assert (index >= 0) && (index < map.size());

        int i = 0;
        List<Map.Entry<K, V>> rest = new ArrayList<Map.Entry<K, V>>();
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (i++ >= index) {
                rest.add(entry);
            }
        }
        map.put(key, value);
        for (int j = 0; j < rest.size(); j++) {
            Map.Entry<K, V> entry = rest.get(j);
            map.remove(entry.getKey());
            map.put(entry.getKey(), entry.getValue());
        }
    }
}