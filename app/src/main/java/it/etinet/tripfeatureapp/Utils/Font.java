package it.etinet.tripfeatureapp.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class Font extends TextView {

    public Font(Context context) {
        super(context);
        setFont();
    }
    public Font(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }
    /*public Font(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }*/

    private void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(),"Roboto-Regular.ttf");
        setTypeface(font, Typeface.NORMAL);
    }
}