package it.etinet.tripfeatureapp.Utils;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

import it.etinet.tripfeatureapp.Vars.StaticVar;

public class Terms {

    private String EULA_PREFIX = "eula_";
    private Activity mActivity;

    public Terms(Activity context) {
        mActivity = context;
    }

    private PackageInfo getPackageInfo() {
        PackageInfo pi = null;
        try {
            pi = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pi;
    }

    public void show() {
        PackageInfo versionInfo = getPackageInfo();

        // the eulaKey changes every time you increment the version number in the AndroidManifest.xml
        final String eulaKey = EULA_PREFIX + versionInfo.versionCode;
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
            // Show the Eula
            String title = "TERMINI E CONDIZIONI GENERALI DI UTILIZZO DI TRIP CITY MAP";

            //Includes the updates as well so users know what changed.
            String message = StaticVar.terms;


            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, new Dialog.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  Mark this version as read.
                            //  SharedPreferences.Editor editor = prefs.edit();
                            //  editor.putBoolean(eulaKey, false);
                            //  editor.commit();
                            dialogInterface.dismiss();
                        }
                    });
            builder.create().show();

    }

}