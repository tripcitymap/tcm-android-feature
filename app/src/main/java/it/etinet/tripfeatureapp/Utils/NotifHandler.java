package it.etinet.tripfeatureapp.Utils;


public class NotifHandler {

    private static final String TABLE_NOTIF = "notif";
    private static final String NOTIF_ID = "id";
    private static final String NOTIF_TITLE = "titolo_notifica";
    private static final String NOTIF_DATE = "data_notifica";
    private static final String NOTIF_DESC = "descrizione_notifica";
    private static final String NOTIF_TYPE = "tipologia_notifica";
    private static final String NOTIF_OWNER = "owner_notifica";


}
