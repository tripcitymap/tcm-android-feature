package it.etinet.tripfeatureapp.Utils;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.etinet.tripfeatureapp.Model.Address;
import it.etinet.tripfeatureapp.Model.Category;
import it.etinet.tripfeatureapp.Model.Channel;
import it.etinet.tripfeatureapp.Model.GeoC;
import it.etinet.tripfeatureapp.Model.GeoObject;
import it.etinet.tripfeatureapp.Model.LiteChannels;
import it.etinet.tripfeatureapp.Model.NewMessage;
import it.etinet.tripfeatureapp.Model.News;
import it.etinet.tripfeatureapp.Model.SpecialField;
import it.etinet.tripfeatureapp.Model.TimeInfo;
import it.etinet.tripfeatureapp.Model.Update;
import it.etinet.tripfeatureapp.Model.mCategory;
import it.etinet.tripfeatureapp.Model.oldMessage;
import it.etinet.tripfeatureapp.ModelSpecial.ObjectBrand;
import it.etinet.tripfeatureapp.ModelSpecial.ObjectFuelPrice;

public class ReverseConverter {

    /*public GeoObject iNeedObj (String jsonString) {
        GeoObject geo = null;
        try {
            geo = new ObjectMapper().readValue(jsonString, GeoObject.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return geo;
    }*/

    public GeoObject iNeedObj (String jsonString) {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, GeoObject.class);
    }

    public Category iNeedCategories (String jsonString) {
        Category cat = null;
        try{
            cat = new ObjectMapper().readValue(jsonString,Category.class);
        } catch(IOException e){
            e.printStackTrace();
        }
        return cat;
    }
    public Channel iNeedChannel (String jsonString) {
        Channel channel = null;
        try{
            channel = new ObjectMapper().readValue(jsonString,Channel.class);
        }catch(IOException e) {
            e.printStackTrace();
        }
        return channel;
    }
    public LiteChannels iNeedLiteChannel (String jsonString) {
        LiteChannels channel = null;
        try{
            channel = new ObjectMapper().readValue(jsonString,LiteChannels.class);
        }catch(IOException e) {
            e.printStackTrace();
        }
        return channel;
    }
    public GeoC myLocation (String jsonString) {
        GeoC address = null;
        try {
            address = new ObjectMapper().readValue(jsonString,GeoC.class);
        }catch (IOException e){
            e.printStackTrace();
        }
        return address;
    }
    public News iNeedNews (String jsonString) {
        News news = null;
        try {
            news = new ObjectMapper().readValue(jsonString,News.class);
        }catch (IOException e){
            e.printStackTrace();
        }
        return news;
    }
    public mCategory iNeedmCategory (String jsonString) {
        mCategory mCategory = null;
        try {
            mCategory = new ObjectMapper().readValue(jsonString,mCategory.class);
        }catch (IOException e){
            e.printStackTrace();
        }
        return mCategory;
    }
    public oldMessage iNeedOldMessage (String jsonString) {
        oldMessage mOldMessage = null;
        try {
            mOldMessage = new ObjectMapper().readValue(jsonString, oldMessage.class);
        }catch (IOException e){
            e.printStackTrace();
        }
        return mOldMessage;
    }
    public NewMessage iNeedNewMessage (String jsonString) {
        NewMessage mNewMEssage = null;
        try{
            mNewMEssage = new ObjectMapper().readValue(jsonString, NewMessage.class);
        }catch (IOException e){
            e.printStackTrace();
        }
        return mNewMEssage;
    }

    public Update iNeedUpdate (String jsonString) {
        Update update = null;
        try {
            update = new ObjectMapper().readValue(jsonString, Update.class);
        }catch (IOException e){
            e.printStackTrace();
        }
        return update;
    }
    // Test
    public GeoObject usingGson (String jsonString) {
        GeoObject geo = null;
        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();

        JSONObject j;

        try{
            j = new JSONObject(jsonString);
            geo = gson.fromJson(j.toString(),GeoObject.class);
        }catch (Exception e )
        {
            e.printStackTrace();
        }
        return geo;

    }

    // Custom deserializer for custom GeoObjects.
    public List<GeoObject> iNeedCustomGeoObject (String jsonString) {
        List<GeoObject> listGeoObject = new ArrayList<GeoObject>();
        try {
            JSONArray jsonArrayPoi = new JSONArray(jsonString);
            for (int j = 0; j < jsonArrayPoi.length(); j++){
                JSONObject g = jsonArrayPoi.getJSONObject(j);
                GeoObject geoObject = new GeoObject();

                if(g.has("categoryId"))
                    geoObject.setCategoryId(g.getString("categoryId"));
                if(g.has("description"))
                    geoObject.setDescription(g.getString("description"));
                if(g.has("featured"))
                    geoObject.setFeatured(Boolean.parseBoolean(g.getString("featured")));
                if(g.has("id"))
                    geoObject.setId(g.getString("id"));
                if(g.has("name"))
                    geoObject.setName(g.getString("name"));
                if(g.has("origin"))
                    geoObject.setOrigin(g.getString("origin"));
                if(g.has("premiumPercentage"))
                    geoObject.setPremiumPercentage(Float.parseFloat(g.getString("premiumPercentage")));
                if(g.has("publicURL"))
                    geoObject.setPublicURL(g.getString("publicURL"));
                if(g.has("typology"))
                    geoObject.setTypology(g.getString("typology"));

                //Working on Address
                if(g.has("address")){
                    JSONObject adr = g.getJSONObject("address");
                    Address gAddress = new Address(); // init obj
                    if(adr.has("addressName"))
                        gAddress.setAddressName(adr.getString("addressName"));
                    if(adr.has("province"))
                        gAddress.setProvince(adr.getString("province"));
                    if(adr.has("city"))
                        gAddress.setCity(adr.getString("city"));
                    if(adr.has("country"))
                        gAddress.setCountry(adr.getString("country"));
                    if(adr.has("provinceAbbreviation"))
                        gAddress.setProvinceAbbreviation(adr.getString("provinceAbbreviation"));
                    if(adr.has("region"))
                        gAddress.setRegion(adr.getString("region"));
                    if(adr.has("civicNumber"))
                        gAddress.setCivicNumber(adr.getString("civicNumber"));
                    if(adr.has("zip"))
                        gAddress.setZip(adr.getString("zip"));
                    geoObject.setAddress(gAddress);//Set parsed Address on the current GeoObject
                }

                // Working on channelsID
                if(g.has("channelsId")){
                    JSONArray jsonChannelsId = g.optJSONArray("channelsId");
                    List<String> channelsIds = new ArrayList<String>();
                    for (int i = 0; i < jsonChannelsId.length(); i++) {
                       channelsIds.add(i,jsonChannelsId.getString(i));
                    }
                    String [] channelsId = new String[channelsIds.size()];
                    for (int i = 0; i < channelsIds.size(); i++) {
                        channelsId[i] = channelsIds.get(i);
                    }
                    geoObject.setChannelId(channelsId); //Set Channels IDS to the poi.
                }

                //Working on GeoInfo
                if(g.has("geoInfo")){
                }
            }
        }catch (JSONException e) {
        return null;
        }
        return listGeoObject;
    }

    // Parse SpecialFields

    /**
     *
     * @param jsonString pass me GeoObject
     * @return Array of SpecialFields, to add to the current GeoObject
     */
    public SpecialField [] parseSpecialFields(String jsonString){
        //This class will parse the special field of a GeoObject if it have one.
        SpecialField [] mSpecialFields = null;
        //Create the json Object from String
        try {
            JSONObject mJsonObject = new JSONObject(jsonString);
            if(mJsonObject.has("specialField")) { // Another control, it should already have it.
                //TODO Here, we must work with the JSONObject from JSONArray of the SpecialField
                JSONArray jsonArraySpecialFields = mJsonObject.getJSONArray("specialField");
                mSpecialFields = new SpecialField[jsonArraySpecialFields.length()];
                for (int i = 0; i < jsonArraySpecialFields.length(); i++) {
                    mJsonObject = jsonArraySpecialFields.getJSONObject(i);
                    SpecialField mSpecialField = new SpecialField();
                    if(mJsonObject.has("description"))
                        mSpecialField.setDescription(mJsonObject.getString("description"));
                    if(mJsonObject.has("internalID"))
                        mSpecialField.setInternalID(mJsonObject.getString("internalID"));
                    if(mJsonObject.has("name"))
                        mSpecialField.setName(mJsonObject.getString("name"));
                    if(mJsonObject.has("order"))
                        mSpecialField.setOrder(Integer.parseInt(mJsonObject.getString("order")));
                    if(mJsonObject.has("representation"))
                        mSpecialField.setRepresentation(Integer.parseInt(mJsonObject.getString("representation")));
                    if(mJsonObject.has("typology"))
                        mSpecialField.setTypology(mJsonObject.getString("typology"));
                    // Working on Time info.
                    if(mJsonObject.has("timeInfo")){
                        JSONObject mTimeInfo = mJsonObject.optJSONObject("timeInfo");
                        TimeInfo gTime = new TimeInfo();
                        if(mTimeInfo.has("creationDate"))
                            gTime.setCreationDate(mTimeInfo.getString("creationDate"));
                        if(mTimeInfo.has("lastUpdated"))
                            gTime.setLastUpdated(mTimeInfo.getString("lastUpdated"));
                        mSpecialField.setTimeInfo(gTime);//Set parsed time info on StpecialField object.
                    }
                    //Now working on value, lets parse them.
                    if(mSpecialField.getInternalID() != null && mJsonObject.has("value")){ //Means it have Internal + value
                        if(mSpecialField.getInternalID().equals("sp_a1")){// Stile Architettonico
                            mSpecialField.setValue(mJsonObject.getString("value"));
                        }else if(mSpecialField.getInternalID().equals("sp_a2")){
                            mSpecialField.setValue(Integer.parseInt(mJsonObject.getString("value")));//Anno Costruzione
                        }else if(mSpecialField.getInternalID().equals("sp_a3")){
                            mSpecialField.setValue(mJsonObject.getString("value"));//Architetto
                        }else if(mSpecialField.getInternalID().equals("sp_a4")){
                            mSpecialField.setValue(Float.parseFloat(mJsonObject.getString("value")));//Altezza
                        }else if(mSpecialField.getInternalID().equals("sp_a5")){
                            mSpecialField.setValue(Integer.parseInt(mJsonObject.getString("value")));//Capienza
                        }else if(mSpecialField.getInternalID().equals("sp_a6")){
                            mSpecialField.setValue(Float.parseFloat(mJsonObject.getString("value")));//Area
                        }else if(mSpecialField.getInternalID().equals("sp_a7")){
                            mSpecialField.setValue(mJsonObject.getString("value"));//Tipologia di cucina
                        }else if(mSpecialField.getInternalID().equals("sp_a8")){
                            mSpecialField.setValue(mJsonObject.getString("value"));//Categoria Hotel
                        }else if(mSpecialField.getInternalID().equals("sp_a9")){
                            mSpecialField.setValue(null);//TODO Not ready yet.
                        }else if(mSpecialField.getInternalID().equals("sp_b1")){
                            mSpecialField.setValue(mJsonObject.getString("value"));//TODO Not ready yet.
                        }else if(mSpecialField.getInternalID().equals("sp_b2")){
                            mSpecialField.setValue(mJsonObject.getString("value"));//Prezzo medio camera doppia
                        }else if(mSpecialField.getInternalID().equals("sp_b4")){
                            mSpecialField.setValue(mJsonObject.getString("value"));//Prezzo medio ora
                        }else if(mSpecialField.getInternalID().equals("sp_b5")){
                            mSpecialField.setValue(mJsonObject.getString("value"));//Prezzo di ingresso
                        }else if(mSpecialField.getInternalID().equals("sp_c3")){
                            mSpecialField.setValue(mJsonObject.getString("value"));//Orario need to be parsed by osm.
                        }else if(mSpecialField.getInternalID().equals("sp_c2")){
                            mSpecialField.setValue(mJsonObject.getJSONArray("value"));//Pagamenti accettati
                        }else if(mSpecialField.getInternalID().equals("sp_b3")){
                            //Parse the object-fuelprice
                            JSONArray objFPArray = mJsonObject.getJSONArray("value");
                            ObjectFuelPrice [] arrayOfFuelPrice;
                            //Errore nell'inizializzazione di array... lo fa sempre.
                            arrayOfFuelPrice = new ObjectFuelPrice[objFPArray.length()];
                            for (int k = 0; k < objFPArray.length(); k++) {
                                ObjectFuelPrice objectFuelPrice = new ObjectFuelPrice();
                                JSONObject jObj = objFPArray.getJSONObject(k);
                                if(jObj.has("dataAggiornamento")){
                                    objectFuelPrice.setDataAggiornamento(jObj.getString("dataAggiornamento"));
                                }
                                if(jObj.has("isSelf")){
                                    objectFuelPrice.setSelf(Boolean.parseBoolean(jObj.getString("isSelf")));
                                }
                                if(jObj.has("prezzo")){
                                    objectFuelPrice.setPrezzo(Double.parseDouble(jObj.getString("prezzo")));
                                }
                                if(jObj.has("tipoCarburante")){
                                    objectFuelPrice.setTipoCarburante(jObj.getString("tipoCarburante"));
                                }
                                arrayOfFuelPrice[k] = objectFuelPrice;
                            }
                            mSpecialField.setValue(arrayOfFuelPrice);
                        }else if(mSpecialField.getInternalID().equals("sp_c1")){
                            //Working on ObjectHours
                            mSpecialField.setValue(mJsonObject.getString("value"));
                        }else if (mSpecialField.getInternalID().equals("sp_c2")){
                            //Working on pagamenti Accettati
                            // Working on Raccolta rifiuti - MultiValue - Array
                            String[] mMultiValues = new String[0];
                            JSONArray multiValue = mJsonObject.getJSONArray("value");
                            mMultiValues = new String[multiValue.length()];
                            for (int y = 0; y < multiValue.length(); y++) {
                                mMultiValues [y] = multiValue.getString(y);
                            }
                            mSpecialField.setValue(mMultiValues);
                        }else if(mSpecialField.getInternalID().equals("sp_d1")){
                            // Working on object-brand
                            JSONObject mObj = mJsonObject.getJSONObject("value");
                            ObjectBrand objectBrand = new ObjectBrand();
                            //FixMe commented part. (2 Check)
                            /*if(mObj.has("setIconUrl")){
                                objectBrand.setIconUrl(mObj.getString("icon"));
                            }
                            if(mObj.has("name")){
                                objectBrand.setName(mObj.getString("name"));
                            }*/

                            mSpecialField.setValue(objectBrand);
                        }else if (mSpecialField.getInternalID().equals("sp_e1")){
                            // Working on Raccolta rifiuti - MultiValue - Array
                            String[] mMultiValues;
                            JSONArray multiValue = mJsonObject.getJSONArray("value");
                            mMultiValues = new String[multiValue.length()];
                            for (int j = 0; j < multiValue.length(); j++) {
                                mMultiValues [j] = multiValue.getString(j);
                            }
                            mSpecialField.setValue(mMultiValues);
                        }
                    }
                    mSpecialFields[i] = mSpecialField;
                }
            }
        } catch (JSONException e) {
            //Could not be able to get the json object
            e.printStackTrace();//TODO Delete this part in prod.
            return null;
        }
        return mSpecialFields;
    }
}