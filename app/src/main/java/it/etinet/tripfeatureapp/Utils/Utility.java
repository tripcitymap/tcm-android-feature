package it.etinet.tripfeatureapp.Utils;


import it.etinet.tripfeatureapp.Model.GeoObject;
import it.etinet.tripfeatureapp.Model.ShareContent;

public class Utility {

    public ShareContent buildshareContent (GeoObject geoObject, String type){
        //Build the shareContent from a geoObject
        ShareContent shareContent = new ShareContent();
        if(geoObject != null){
            if(geoObject.getAddress() != null){
                geoObject.setAddress(geoObject.getAddress());
            }
            shareContent.setTitle(geoObject.getName());
            if("fb".equals(type)) {
                shareContent.setDescription(geoObject.getDescription().substring(0, Math.min(geoObject.getDescription().length(), 200)));//Take 200 chars from String.
            }
        }
        return shareContent;
    }
}