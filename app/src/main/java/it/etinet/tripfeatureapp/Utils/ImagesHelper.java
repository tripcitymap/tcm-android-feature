package it.etinet.tripfeatureapp.Utils;


import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class ImagesHelper {


    FileInputStream fin;

    public InputStream getStreamFromFile(String fileName, String type) {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/it.etinet.tripfeatureapp/data/",type+fileName+".tcm");
        if(file.exists()){
        try {
            fin = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        }else {
            //FILE DOES NOT EXIST USE GENERIC.
        }
        return fin;
    }

    public int countFiles (){
        int count;
        File file = new File (Environment.getExternalStorageDirectory().getAbsolutePath()+ "/Android/data/it.etinet.tripfeatureapp/data/");
        if(file.isDirectory()){
            count = file.listFiles().length;
        }else {
            count = -1;
        }
        return count;
    }
}
