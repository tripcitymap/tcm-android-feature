package it.etinet.tripfeatureapp.Utils;


import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReadFileStatic {

    public List ReadFile (String nameFile){
        List<String> result = new ArrayList<String>();

        try {
            // File file1 = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/it.etinet.tripfeatureapp/", NOTES);
            File file1 = new File(Environment.getExternalStorageDirectory().getAbsolutePath() , nameFile);

            BufferedReader br = new BufferedReader(new FileReader(file1));
            String line ;
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
            br.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return result;
    }
}
