package it.etinet.tripfeatureapp.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import it.etinet.tripfeatureapp.Vars.StaticVars2;


public class ResizeSVG {

    public Drawable scaleSVG (Context context , Drawable image , int screenWidth , int screenHeight , double precentage) {
        double scaleFactor = 1;
        if(screenWidth==800){                             //tablet 10 policci
            scaleFactor = 0.75 * precentage;
        }else if(screenWidth==600){                             //tablet 7 policci
            scaleFactor = 0.35 * precentage;
        } else if(screenHeight < 850){                               //s3min
            if(StaticVars2.svgDet){
                scaleFactor = 1 * precentage;   //Edited part for resized SVG
                StaticVars2.svgDet = false;
            }else {
                scaleFactor = 0.50 * precentage;
            }
        }else if(screenHeight < 1000) {                       //s4mini
            scaleFactor = 0.50 * precentage;
        }else if(screenHeight < 1400){                        //s3
            scaleFactor = 0.75 * precentage;
        }
        else if(screenHeight > 1400){                       //nexus5
            scaleFactor = 1 * precentage;
        }

        if ((image == null) || !(image instanceof BitmapDrawable)) {
            return image;
        }

        Bitmap b = ((BitmapDrawable)image).getBitmap();

        int sizeX = (int) Math.round(image.getIntrinsicWidth() * scaleFactor);
        int sizeY = (int) Math.round(image.getIntrinsicHeight() * scaleFactor);
        Bitmap bitmapResized;
        if(b != null) {
            bitmapResized = Bitmap.createScaledBitmap(b, sizeX, sizeY, false);
            image = new BitmapDrawable(context.getResources(), bitmapResized);
        }else {
            return null;
        }

        return image;

    }

    public Bitmap scaleSVGBitmap (Context context , Bitmap image , int screenWidth , int screenHeight , double precentage) {
        double scaleFactor = 1;
        if(screenWidth==800){                  //tablet 10 policci
            scaleFactor = 0.50 * precentage;
        }else if(screenWidth==600){            //tablet 7 policci
            scaleFactor = 0.50 * precentage;
        } else if(screenHeight < 850){         //s3min
            if(StaticVars2.svgDet){
                scaleFactor = 1 * precentage;   //Edited part for resized SVG
                StaticVars2.svgDet = false;
            }else {
                scaleFactor = 0.50 * precentage;
            }
        }else if(screenHeight < 1000) {        //s4mini
            if(StaticVars2.svgDet){
                scaleFactor = 1 * precentage;   //Edited part for resized SVG
                StaticVars2.svgDet = false;
            }else {
                scaleFactor = 0.50 * precentage;
            }
        }else if(screenHeight < 1400){         //s3
            scaleFactor = 0.75 * precentage;
        }
        else if(screenHeight > 1400){          //nexus5
            scaleFactor = 1 * precentage;
        }

        Bitmap resized = Bitmap.createScaledBitmap(image,(int)(image.getWidth()*scaleFactor), (int)(image.getHeight()*scaleFactor), true);

        return resized;

    }
}
