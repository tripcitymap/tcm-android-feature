package it.etinet.tripfeatureapp.Utils;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import it.etinet.tripfeatureapp.Model.Address;
import it.etinet.tripfeatureapp.Vars.StaticVar;
import it.etinet.tripfeatureapp.Vars.StaticVars2;

public class GeneralUtil {

    public int parseMeColor (String color) {
        String [] mColor = color.split("\\|");
        String andColor = mColor[1];
        String [] iNeedColor = andColor.split(":");

        return Color.parseColor(iNeedColor[1]);
    }

    //merge two bitmap
    public Bitmap mergeBitmaps (Bitmap bm1,Bitmap bm2) {
        Bitmap newBitmap;

        int w;
        if(bm1.getWidth() >= bm2.getWidth()){
            w = bm1.getWidth();
        }else{
            w = bm2.getWidth();
        }

        int h;
        if(bm1.getHeight() >= bm2.getHeight()){
            h = bm1.getHeight();
        }else{
            h = bm2.getHeight();
        }

        Bitmap.Config config = bm2.getConfig();
        if(config == null){
            config = Bitmap.Config.ARGB_8888;
        }

        newBitmap = Bitmap.createBitmap(w, h, config);
        Canvas newCanvas = new Canvas(newBitmap);

        Paint paint = new Paint();
        // paint.setAlpha(128);
        if(StaticVars2.mergeScreenWidth==800){
            newCanvas.drawBitmap(bm1, 3, 3, paint);
        }else if(StaticVars2.mergeScreenWidth==600){
            newCanvas.drawBitmap(bm1, 3, 3, paint);
        }else if(StaticVars2.mergeScreenHeight < 1000){
            newCanvas.drawBitmap(bm1, 3, 3, paint);
        }else if(StaticVars2.mergeScreenHeight < 1400){
            newCanvas.drawBitmap(bm1, 3, 3, paint);
        }else if(StaticVars2.mergeScreenHeight > 1400){
            newCanvas.drawBitmap(bm1, 3, 3, paint);
        }
        newCanvas.drawBitmap(bm2, 0, 0, null); //CROWN drwan at 0,0
        return newBitmap;
    }

    //Build a list
    public List<String> buildMeList (String jsonString) {
        List<String> mList = new ArrayList<String>();
        if (jsonString != null && !jsonString.isEmpty()) {
            try {
                //jsonString = "";
                char c = jsonString.charAt(0);
                String testingArray = ""+c;
                if("{".equals(testingArray)){
                    JSONObject jsonObject  = new JSONObject(jsonString);
                    mList.add(0,jsonObject.toString());
                }else if("[".equals(testingArray)) {
                     JSONArray jsonArrayPoi = new JSONArray(jsonString);
                     for (int j = 0; j < jsonArrayPoi.length(); j++){
                         JSONObject p = jsonArrayPoi.getJSONObject(j);
                        mList.add(j,p.toString());
                    }
                }
                } catch (JSONException e) {
                    e.printStackTrace();
                    //return null;
                }
            return mList;
        }
        return null;
    }

    public void iNeedBDInformationsByName (String [] comuni, String ricercato){
        String[] s;
        for(int i=0;i<comuni.length;i++){
            s = comuni[i].split(",");
            String actualCheck = s[0].replaceAll(" ", "_").replaceAll("'","_").replaceAll("ò","o").replaceAll("é","e").replaceAll("è","e").replaceAll("ì","i").replaceAll("à","a").replace("ù","u");
            if(actualCheck.equals(ricercato)){
                StaticVars2.ricercaDBimmagine = s[1];
                StaticVars2.ricercaDBId = s[2];
            }
        }
    }

    //Unzip
    public boolean unpackZip(String path, String zipname) {
        InputStream is;
        ZipInputStream zis;
        try
        {
            is = new FileInputStream(path + zipname);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;

            while((ze = zis.getNextEntry()) != null)
            {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int count;

                String filename = ze.getName();
                FileOutputStream fout = new FileOutputStream(path + filename);

                // reading and writing
                while((count = zis.read(buffer)) != -1)
                {
                    baos.write(buffer, 0, count);
                    byte[] bytes = baos.toByteArray();
                    fout.write(bytes);
                    baos.reset();
                }

                fout.close();
                zis.closeEntry();
            }
            zis.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public String addressParser (Address address){
        String composedAddress;

        if(address != null) {
            String addressName = address.getAddressName();
            String city = address.getCity();
            String zip = address.getZip();
            String civic = address.getCivicNumber();
            String province = address.getProvince();
            String region = address.getRegion();
            String proviceAbbreviation = address.getProvinceAbbreviation();


            if (addressName == null || addressName.equals("") || addressName.equals("null")) {
                addressName = "";
            }

            if (city == null || city.equals("") || city.equals("null")) {
                city = "";
            }

            if (zip == null || zip.equals("") || zip.equals("null")) {
                zip = "";
            }

            if (civic == null || civic.equals("") || civic.equals("null")) {
                civic = "";
            }

            if (province == null || province.equals("") || province.equals("null")) {
                province = "";
            }

            if (region == null || region.equals("") || region.equals("null")) {
                region = "";
            }

            if (proviceAbbreviation == null || proviceAbbreviation.equals("") || proviceAbbreviation.equals("null")) {
                proviceAbbreviation = "";
            }

            //Address Name, Civic, Comune - Zip (AbbrevProvincia)
            if (addressName.isEmpty()) {
                if (civic.isEmpty()) {
                    if (city.isEmpty()) {
                        if (zip.isEmpty()) {
                            if (province.isEmpty()) {
                                //Tutti sono vuoti posizione ND
                                composedAddress = "ND";
                            } else {
                                //c'è l'abbreviazione
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = "(" + province + ")";
                                } else {
                                    composedAddress = "(" + proviceAbbreviation + ")";
                                }
                            }
                        } else {
                            // c'è lo zip
                            if (province.isEmpty()) {
                                composedAddress = zip;
                            } else {
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = zip + "  (" + province + ")";
                                } else {
                                    composedAddress = zip + "  (" + proviceAbbreviation + ")";
                                }
                            }
                        }
                    } else {
                        //c'è la città
                        if (zip.isEmpty()) {
                            //c'è la città ma non lo zip
                            if (province.isEmpty()) {
                                //c'è la città, non c'è lo zip nemmeno la province
                                composedAddress = city;
                            } else {
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = city + " -  (" + province + ")";
                                } else {
                                    composedAddress = city + " -  (" + proviceAbbreviation + ")";
                                }
                            }
                        } else {
                            //c'è lo zip
                            if (province.isEmpty()) {
                                // c'è lo zip ma non la provincia
                                composedAddress = city + " - " + zip;
                            } else {
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = city + " - " + zip + "  (" + province + ")";
                                } else {
                                    composedAddress = city + " - " + zip + "  (" + proviceAbbreviation + ")";
                                }
                            }
                        }
                    }
                } else {
                    //c'è il civic
                    if (city.isEmpty()) {
                        //non c'è il comune
                        if (zip.isEmpty()) {
                            //non c'è lo zip
                            if (province.isEmpty()) {
                                composedAddress = civic;
                            } else {
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = civic + " - (" + province + ")";
                                } else {
                                    composedAddress = civic + " - (" + proviceAbbreviation + ")";
                                }
                            }
                        } else {
                            // ci sono civic, zip, c'è la provincia
                            if (province.isEmpty()) {
                                composedAddress = civic + " - " + zip;
                            } else {
                                // ci sono provincia, zip e civic
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = civic + " - " + zip + " (" + province + ")";
                                } else {
                                    composedAddress = civic + " - " + zip + " (" + proviceAbbreviation + ")";
                                }
                            }
                        }
                    } else {
                        // c'è il civico e la città
                        if (zip.isEmpty()) {
                            //non c'è lo zip
                            if (province.isEmpty()) {
                                // non c'è ne zip ne provincia
                                composedAddress = civic + ", " + city;
                            } else {
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = civic + ", " + city + " - " + " (" + province + ")";
                                } else {
                                    composedAddress = civic + ", " + city + " - " + " (" + proviceAbbreviation + ")";
                                }
                            }
                        } else {
                            if (province.isEmpty()) {
                                // zip citta civico no provincia
                                composedAddress = civic + ", " + city + " - " + zip;
                            } else {
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = civic + ", " + city + " - " + zip + " (" + province + ")";
                                } else {
                                    composedAddress = civic + ", " + city + " - " + zip + " (" + proviceAbbreviation + ")";
                                }
                            }
                        }
                    }
                }
            } else {
                // c'è l'address
                if (civic.isEmpty()) {
                    // c'è address no civic
                    if (city.isEmpty()) {
                        //c'è address no civic no comune
                        if (zip.isEmpty()) {
                            // c'è address no civic no comune no zip
                            if (province.isEmpty()) {
                                composedAddress = addressName;
                            } else {
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = addressName + " -  (" + province + ")";
                                } else {
                                    composedAddress = addressName + " -  (" + proviceAbbreviation + ")";
                                }
                            }
                        } else {
                            //c'è address e zip no civic no city ? province?
                            if (province.isEmpty()) {
                                //address + zip
                                composedAddress = addressName + " - " + zip;
                            } else {
                                //address + zip + province
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = addressName + " - " + zip + " (" + province + ")";
                                } else {
                                    composedAddress = addressName + " - " + zip + " (" + proviceAbbreviation + ")";
                                }
                            }
                        }
                    } else {
                        // c'è address e city NO civic, control zip e province
                        if (zip.isEmpty()) {
                            if (province.isEmpty()) {
                                composedAddress = addressName + ", " + city;
                            } else {
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = addressName + ", " + city + " - (" + province + ")";
                                } else {
                                    composedAddress = addressName + ", " + city + " - (" + proviceAbbreviation + ")";
                                }
                            }
                        } else {
                            if (province.isEmpty()) {
                                composedAddress = addressName + ", " + city + " - " + zip;
                            } else {
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = addressName + ", " + city + " - " + zip + " (" + province + ")";
                                } else {
                                    composedAddress = addressName + ", " + city + " - " + zip + " (" + proviceAbbreviation + ")";
                                }
                            }
                        }
                    }
                } else {
                    // c'è address e civic
                    if (city.isEmpty()) {
                        // no city
                        if (zip.isEmpty()) {
                            if (province.isEmpty()) {
                                composedAddress = addressName + ", " + civic;
                            } else {
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = addressName + ", " + civic + " -  (" + province + ")";
                                } else {
                                    composedAddress = addressName + ", " + civic + " -  (" + proviceAbbreviation + ")";
                                }
                            }
                        } else {
                            if (province.isEmpty()) {
                                composedAddress = addressName + ", " + civic + " - " + zip;
                            } else {
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = addressName + ", " + civic + " - " + zip + " (" + province + ")";
                                } else {
                                    composedAddress = addressName + ", " + civic + " - " + zip + " (" + proviceAbbreviation + ")";
                                }
                            }
                        }
                    } else {
                        //address, civic, city
                        if (zip.isEmpty()) {
                            if (province.isEmpty()) {
                                composedAddress = addressName + ", " + civic + ", " + city;
                            } else {
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = addressName + ", " + civic + ", " + city + " -  (" + province + ")";
                                } else {
                                    composedAddress = addressName + ", " + civic + ", " + city + " -  (" + proviceAbbreviation + ")";
                                }
                            }
                        } else {
                            if (province.isEmpty()) {
                                composedAddress = addressName + ", " + civic + ", " + city + " - " + zip;
                            } else {
                                if (proviceAbbreviation.isEmpty()) {
                                    composedAddress = addressName + ", " + civic + ", " + city + " - " + zip + " (" + province + ")";
                                } else {
                                    composedAddress = addressName + ", " + civic + ", " + city + " - " + zip + " (" + proviceAbbreviation + ")";
                                }
                            }
                        }
                    }
                }
            }
        }else {
            composedAddress = "ND";
        }
        //Address Name, Civic, Comune - Zip (AbbrevProvincia)
        return composedAddress;
    }

    public String detHAddressParser (Address address){
        String composedURL;

        String addressName ;
        String civicNumber;

        if(address.getAddressName() == null){
            addressName = "";
        }else {
            addressName = address.getAddressName();
        }

        if(address.getCivicNumber() == null) {
            civicNumber = "";
        }else {
            civicNumber = address.getCivicNumber();
        }

        if(addressName.isEmpty()){
           composedURL = "(Indirizzo sconosciuto)";
        }else {
            if(civicNumber.isEmpty()){
                composedURL  = addressName;
            }else {
                composedURL = addressName + ", " + civicNumber;
            }
        }

        return composedURL;
    }

    public String detLAddressParser (Address address){
        String composedURL;
        String zip;
        String city;
        String province;
        String provinceAbbreviation;

        if(address.getZip() == null){
            zip = "";
        }else {
            zip = address.getZip();
        }
        if(address.getCity() == null){
            city = "";
        }else {
            city = address.getCity();
        }
        if(address.getProvince() == null){
            province = "";
        }else {
            province = address.getProvince();
        }
        if(address.getProvinceAbbreviation() == null){
            provinceAbbreviation = "";
        }else {
            provinceAbbreviation = address.getProvinceAbbreviation();
        }

        if(zip.isEmpty()){
            if(city.isEmpty()){
                if(province.isEmpty()){
                    composedURL = "ND";
                }else {
                    if(provinceAbbreviation.isEmpty()) {
                        composedURL = "(" + province + ")";
                    }else {
                        composedURL = "(" + provinceAbbreviation + ")";
                    }
                }
            }else {
                if (province.isEmpty()){
                    composedURL = city;
                }else {
                    if(provinceAbbreviation.isEmpty()) {
                        composedURL = city + " (" + province + ")";
                    }else {
                        composedURL = city + " (" + provinceAbbreviation + ")";
                    }
                }
            }
        }else {
            if(city.isEmpty()){
                if(province.isEmpty()){
                    composedURL = zip;
                }else {
                    if(provinceAbbreviation.isEmpty()) {
                        composedURL = zip + " - " + "(" + province + ")";
                    }else {
                        composedURL = zip + " - " + "(" + provinceAbbreviation + ")";
                    }
                }
            }else {
                if(province.isEmpty()){
                    composedURL = zip + " - " + city;
                }else {
                    if(provinceAbbreviation.isEmpty()) {
                        composedURL = zip + " - " + city + " (" + province + ")";
                    }else {
                        composedURL = zip + " - " + city + " (" + provinceAbbreviation + ")";
                    }
                }
            }
        }

        //12038 - Savigliano (CN)
        return composedURL;
    }

    public boolean checkFuelPriceHours (String date){
        boolean ret = true;
        String devicemonthstring;
        String [] data = date.split("T");
        String [] singleValues = data[0].split("-");
        int updatedDay = Integer.parseInt(singleValues[2]);
        int updatedMonth = Integer.parseInt(singleValues[1]);
        Calendar c = Calendar.getInstance();
        int deviceday = c.get(Calendar.DAY_OF_MONTH);
        int devicemonth = c.get(Calendar.MONTH) + 1;

        if (devicemonth > 1 && devicemonth < 10)
            devicemonthstring = "0" + String.valueOf(devicemonth);


        if(devicemonth > updatedMonth) {
            ret = false;
        }else {
            if(deviceday >= updatedDay){
                ret = false;
            }
        }


        return ret;
    }

    public String eventTimeInfoParser (String startDate, String endDate){

        String ret = "";
        String [] splitStartDate = startDate.split("T");
        String [] splitEndDate = endDate.split("T");

        String [] incorrectStartDate = splitStartDate[0].split("-");
        String [] incorrectEndDate = splitEndDate[0].split("-");

        if(incorrectStartDate[2].equals(incorrectEndDate[2])){
            ret = "Il giorno: " + incorrectStartDate[2]+"-"+incorrectStartDate[1]+"-"+incorrectStartDate[0];
        }else {
            ret = "Da " + incorrectStartDate[2]+"-"+incorrectStartDate[1]+"-"+incorrectStartDate[0]+ " a " + incorrectEndDate[2]+"-"+incorrectEndDate[1]+"-"+incorrectEndDate[0];
        }
        return ret;
    }

    public boolean updateChecker (String data) {
        boolean iNeedUpdate = false;
        String [] split = data.split("-");
        int updateDay = Integer.parseInt(split [0]);
        int updateMonth = Integer.parseInt(split [1]);
        int updateYear = Integer.parseInt(split [2]);

        String devicemonthstring = "";
        // Device Timing
        Calendar c = Calendar.getInstance();
        int deviceyear = c.get(Calendar.YEAR);
        int devicemonth = c.get(Calendar.MONTH) + 1;

        if (devicemonth > 1 && devicemonth < 10)
            devicemonthstring = "0" + String.valueOf(devicemonth);

        int deviceday = c.get(Calendar.DAY_OF_MONTH);
        int devicehours = c.get(Calendar.HOUR_OF_DAY);
        int deviceminutes = c.get(Calendar.MINUTE);

        if(deviceyear > updateYear){
            iNeedUpdate = true;
        }else {
            if(devicemonth > updateMonth){
                iNeedUpdate = true;
            }else {
                if(deviceday-updateDay > 4){
                    iNeedUpdate = true;
                }
            }
        }
        return iNeedUpdate;
    }

    public static double getMinValue(double[] array){
        double minValue = array[0];
        for(int i=1;i<array.length;i++){
            if(array[i] < minValue){
                minValue = array[i];
            }
        }
        return minValue;
    }
    // getting the maximum value
    public static double getMaxValue(double[] array){
        double maxValue = array[0];
        for(int i=1;i < array.length;i++){
            if(array[i] > maxValue){
                maxValue = array[i];
            }
        }
        return maxValue;
    }

    public static String getCorrectName (String name){
        String correct = "";
        if(name != null && !"".equals(name)){
            String single;
            String [] split = name.split(" ");
            if(split.length > 1){
                for (String aSplit : split) {
                    if (("di".equals(aSplit)) || ("a".equals(aSplit)) || ("da".equals(aSplit)) || ("al".equals(aSplit)) || ("e".equals(aSplit)) || ("delle".equals(aSplit)) || ("della".equals(aSplit))) {
                        single = aSplit;
                    } else {
                        single = aSplit.substring(0, 1).toUpperCase() + aSplit.substring(1);
                    }
                    if ("".equals(correct)) {
                        correct = single;
                    } else {
                        correct = correct + " " + single;
                    }
                }
            }else {
                correct = split[0].substring(0, 1).toUpperCase() + split[0].substring(1);
            }
        }
        return correct;
    }

    public String correctImage (String path, String from) {
        String add = "";
        if (path != null && !path.isEmpty()) {
            if(path.contains("panoramio")){
                if(path.contains("original")){
                    path = path.replaceAll("original","medium");
                }else if(path.contains("large")){
                    path = path.replaceAll("large","medium");
                }else if(path.contains("small")){
                    path = path.replaceAll("small","medium");
                }else {
                    return path;
                }
            }else if (path.contains("tripfeatureapp.com")) {
                if ("dash".equals(from)) {
                    if (StaticVar.deviceMdpi) {
                        add = "&width=200";
                    } else if (StaticVar.deviceHdpi) {
                        add = "&width=200";
                    } else if (StaticVar.deviceXhdpi) {
                        add = "&width=400";
                    } else if (StaticVar.deviceXxhdpi) {
                        add = "&width=400";
                    } else {
                        add = "&width=200";
                    }
                } else if ("det".equals(from)) {
                    if (StaticVar.deviceMdpi) {
                        add = "&width=100";
                    } else if (StaticVar.deviceHdpi) {
                        add = "&width=200";
                    } else if (StaticVar.deviceXhdpi) {
                        add = "&width=300";
                    } else if (StaticVar.deviceXxhdpi) {
                        add = "&width=400";
                    } else {
                        add = "&width=200";
                    }
                } else if ("list".equals(from)) {
                    if (StaticVar.deviceMdpi) {
                        add = "&width=50";
                    } else if (StaticVar.deviceHdpi) {
                        add = "&width=100";
                    } else if (StaticVar.deviceXhdpi) {
                        add = "&width=200";
                    } else if (StaticVar.deviceXxhdpi) {
                        add = "&width=300";
                    } else {
                        add = "&width=200";
                    }
                } else if ("mainDet".equals(from)) {
                    if (StaticVar.deviceMdpi) {
                        add = "&width=200";
                    } else if (StaticVar.deviceHdpi) {
                        add = "&width=400";
                    } else if (StaticVar.deviceXhdpi) {
                        add = "&width=600";
                    } else if (StaticVar.deviceXxhdpi) {
                        add = "&width=800";
                    } else {
                        add = "&width=400";
                    }
                } else if ("infoMarker".equals(from)) {
                    if (StaticVar.deviceMdpi) {
                        add = "&width=100";
                    } else if (StaticVar.deviceHdpi) {
                        add = "&width=200";
                    } else if (StaticVar.deviceXhdpi) {
                        add = "&width=200";
                    } else if (StaticVar.deviceXxhdpi) {
                        add = "&width=200";
                    } else {
                        add = "&width=200";
                    }
                }
             }
        } else {
            return path + add;
        }
        return path + add;
    }
}