package it.etinet.tripfeatureapp.Utils;


import android.os.Build;

import java.util.Locale;

import it.etinet.tripfeatureapp.Vars.StaticVar;
import it.etinet.tripfeatureapp.Vars.StaticVars2;

public class iErrorCatcher {

    public static String CatchIt (String iErrorCode){

        if(iErrorCode.contains("F")){
        }else {
        }
        return "";
    }

    public void theError (){
        String deviceModel = getDeviceName();
        String actualAppVersion = StaticVar.appVersion;
        int widthPixel = StaticVar.widthPixels;
        int heightPixel = StaticVar.heightPixels;
        String deviceLang = Locale.getDefault().getLanguage();
        float deviceDensity  = StaticVars2.density;
    }

    //The utils down there
    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }
}
