package it.etinet.tripfeatureapp.Utils;


import android.app.Activity;

public class ImageErrorHandler {

    /**
     * Immagini di panoramio possono provenire da:
     *  1 - IlmioComune
     *  2 - Dettaglio di un punto di interesse.
     *  3 - Dalla home screen.
     * */

    private Activity context;
    private String imageUrl;
    private String errorType;
    private String errorCode;
    private String errorSource;


    private ImageErrorHandler (String imageUrl, String errorType, String errorCode, String errorSource, Activity context){
        this.context = context;
        this.imageUrl = imageUrl;
        this.errorType = errorType;
        this.errorCode = errorCode;
        this.errorSource = errorSource;
    }



    /*private  void parseComunication (final Reports report) {
        HashMap<String,String> parseObjectHashMap = new HashMap<String,String>();
        String android_id = Settings.Secure.getString(getApplication().getContentResolver(), Settings.Secure.ANDROID_ID);

        parseObjectHashMap.put("installationId", android_id);
        parseObjectHashMap.put("deviceToken", deviceToken);
        parseObjectHashMap.put("geoId", selected2.getId());
        parseObjectHashMap.put("requestStatus", "pending");

        ParseCloud.callFunctionInBackground("searchForReport", parseObjectHashMap, new FunctionCallback<String>() {
            public void done(String o, ParseException e) {
                if (e == null) {
                    if ("create".equals(o)) {
                        parseCreateNew(report);
                    } else {
                        String[] mArray = new String[1];
                        mArray[0] = o;
                        parseUpdateExist(report, mArray);
                    }
                } else {
                    e.printStackTrace();
                }
            }
        });
    }*/
}
