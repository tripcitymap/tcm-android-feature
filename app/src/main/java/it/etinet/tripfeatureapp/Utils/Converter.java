package it.etinet.tripfeatureapp.Utils;

import com.google.gson.Gson;

import it.etinet.tripfeatureapp.Model.Channel;
import it.etinet.tripfeatureapp.Model.GeoObject;

public class Converter {

    /*public String iNeedaJson (GeoObject obj) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            // convert user object to json string,;
            return mapper.writeValueAsString(obj);
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }*/

    public String iNeedaJson (GeoObject obj){
        Gson gson = new Gson();
        return gson.toJson(obj);
    }

    public String iNeedChannelJson (Channel channel){
        Gson gson = new Gson();
        return gson.toJson(channel);
    }

    /*public String iNeedChannelJson (Channel channel){
        ObjectMapper mapper = new ObjectMapper();
        try {
            // convert user object to json string,;
            return mapper.writeValueAsString(channel);
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }*/

}
