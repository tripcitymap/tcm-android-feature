package it.etinet.tripfeatureapp.Utils;

import java.util.ArrayList;
import java.util.List;

import it.etinet.tripfeatureapp.Model.Photo;
import it.etinet.tripfeatureapp.Model.PhotoGallery;

public class PhotosManager {

    public int howManyPhotos (it.etinet.tripfeatureapp.Model.PhotoGallery[] photoGalleries, Photo mainImage) {
        int totPhoto = 0;
        Photo[] photos = new Photo[]{};

        if(photoGalleries != null) {
            for (int i = 0; i < photoGalleries.length; i++) {
                photos = photoGalleries[i].getPhotos();
            }
        }
        if(mainImage != null && mainImage.getFileURL() !=null && !mainImage.getFileURL().contains("vuota")){
            if(photos != null)
                totPhoto = photos.length + 1;
        }else {
            if(photos != null)
                totPhoto = photos.length;
        }
        return totPhoto;
    }

    public List<String> photosUrl (PhotoGallery [] photoGalleries, Photo mainImage) {
        List<String> photosUrl = new ArrayList<String>();
        Photo[] photos = new Photo[]{};

        if(photoGalleries != null) {
            for (int i = 0; i < photoGalleries.length; i++) {
                photos = photoGalleries[i].getPhotos();
            }
            if(photos != null) {
                for (int i = 0; i < photos.length; i++) {
                    photosUrl.add(i, photos[i].getFileURL());
                }
            }
        }
        // Modifica affinchè l'immagine principale sia la prima della fotogallery.
        if(mainImage != null && !mainImage.getFileURL().contains("vuota")){
            photosUrl.add(0,mainImage.getFileURL());
        }
        return  photosUrl;
    }
}