package it.etinet.tripfeatureapp.Utils;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import it.etinet.tripfeatureapp.Vars.StaticVar;

public class InternetDetector extends BroadcastReceiver{
        boolean cambiato;
        @Override
        public void onReceive( Context context, Intent intent )
        {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
            NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
            NetworkInfo mobNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE );
            if ( activeNetInfo != null )
            {
             if(StaticVar.networkAvailability ){
                 StaticVar.networkAvailability = false;
             }else{
                 cambiato=true;
                 StaticVar.networkAvailability = true;
             }

            }
            if( mobNetInfo != null )
            {
            if(StaticVar.networkAvailability && !cambiato) {
                StaticVar.networkAvailability = false;
            }else{
                StaticVar.networkAvailability = true;
            }
           }
        }

    }