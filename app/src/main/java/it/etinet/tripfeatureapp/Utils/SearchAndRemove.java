package it.etinet.tripfeatureapp.Utils;


import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class SearchAndRemove {

    public SearchAndRemove (){

    }

    public boolean removeFile (){
        String NOTES ="tripfeatureapp.txt";
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/Android/data/it.etinet.tripfeatureapp/", NOTES);
        return file.delete();
    }
    public boolean removePicture(){
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +"/Android/data/it.etinet.tripfeatureapp/"+"FotoComune.jpg");
        return file.delete();
    }
    private String a[];
    public String searchForId(){
        String strPathFile=Environment.getExternalStorageDirectory().getAbsolutePath() +"/Android/data/it.etinet.tripfeatureapp/"+"tripfeatureapp.txt";
        try {
            FileInputStream objFile = new FileInputStream(strPathFile);
            InputStreamReader objReader = new InputStreamReader(objFile);
            BufferedReader objBufferReader = new BufferedReader(objReader);
            StringBuffer objBuffer = new StringBuffer();
            String strLine;
            while ((strLine = objBufferReader.readLine()) != null) {
                objBuffer.append(strLine);
                objBuffer.append("\n");
            }
            a = objBuffer.toString().split("\n");
            objFile.close();
        }
        catch (FileNotFoundException objError) {
            return null;
        }
        catch (IOException objError) {
            return null;
        }
        return a[a.length-1];
    }
}
