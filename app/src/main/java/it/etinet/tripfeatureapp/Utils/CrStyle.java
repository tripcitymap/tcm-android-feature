package it.etinet.tripfeatureapp.Utils;

import android.view.ViewGroup;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Style;
import it.etinet.tripfeatureapp.R;

public class CrStyle {

    public static final int DURATION_INFINITE = -1;
    public static final Style ALERT;
    public static final Style WARN;
    public static final Style CONFIRM;
    public static final Style INFO;
    public static final Style DET;
    public static final Style INFOINFINITE;

    public static final int AlertRed = R.color.alert_red;
    public static final int WarnOrange = R.color.warn_orange;
    public static final int ConfirmGreen = R.color.confirm_green;
    public static final int InfoYellow = R.color.info_yellow;

    private static final int DURATION_SHORT  = 3000;
    private static final int DURATION_MEDIUM = 5000;
    private static final int DURATION_LONG   = 10000;

    public static final Configuration croutonConfigurationShort = new Configuration.Builder()
            .setDuration(DURATION_SHORT).build();
    public static final Configuration croutonConfigurationMedium = new Configuration.Builder()
            .setDuration(DURATION_MEDIUM).build();
    public static final Configuration croutonConfigurationLong = new Configuration.Builder()
            .setDuration(DURATION_LONG).build();
    public static final Configuration croutonConfigurationInfinite = new Configuration.Builder()
            .setDuration(DURATION_INFINITE).build();
    static {
        ALERT   = new Style.Builder()
                .setTextColor(R.color.alert_tcolor)
                .setConfiguration(croutonConfigurationLong)
                .setBackgroundColor(AlertRed)
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .build();
        WARN    = new Style.Builder()
                .setTextColor(R.color.warn_tcolor)
                .setConfiguration(croutonConfigurationShort)
                .setBackgroundColor(WarnOrange)
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .build();
        CONFIRM = new Style.Builder()
                .setTextColor(R.color.confirm_tcolor)
                .setConfiguration(croutonConfigurationLong)
                .setBackgroundColor(ConfirmGreen)
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .build();
        INFO    = new Style.Builder()
                .setTextColor(R.color.info_tcolor)
                .setConfiguration(croutonConfigurationMedium)
                .setBackgroundColor(InfoYellow)
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .build();
        DET     = new Style.Builder()
                .setTextColor(R.color.info_tcolor)
                .setConfiguration(croutonConfigurationInfinite)
                .setBackgroundColor(InfoYellow)
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .build();
        INFOINFINITE    = new Style.Builder()
                .setTextColor(R.color.info_tcolor)
                .setConfiguration(croutonConfigurationInfinite)
                .setBackgroundColor(InfoYellow)
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .build();

    }
}