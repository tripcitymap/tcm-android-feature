package it.etinet.tripfeatureapp.Utils;


import java.util.Random;

import it.etinet.tripfeatureapp.Model.Channel;
import it.etinet.tripfeatureapp.Model.GeoObject;
import it.etinet.tripfeatureapp.Model.mCategory;

public class WelcomeMessage {

    public String getWelcomeMessage (Channel channel, GeoObject [] arrayOfGeoObj) {
        String message;
        int number = 0;
        //FIXME TEST
        channel.setName("Verona");
        //StaticVars2.geoObjectFromDashboard;
        if(arrayOfGeoObj != null){
            for (int i = 0; i < arrayOfGeoObj.length; i++) {
                if(arrayOfGeoObj[i].getAddress().getCity().equals(channel.getName())){
                    //StaticVars2.fromDash = true;
                    //StaticVars2.geoObjectFromDashboard = arrayOfGeoObj[i];
                    number = 3;
                }else {
                    number = 0;//randInt(0,1);
                }
            }
        }
        switch (number){
            case 0 : message = "Lo sapevi che a " + channel.getName() + " attualmente ci sono " + catMessage(channel);
            //"Lo sapevi che a " + channel.getName() + " attualmente ci sono " + channel.getTotalPoi() + " punti di interesse?";
                break;
            case 1 : message = "";//"Lo sapevi che a " + channel.getName() + " " + catNameTotalPoi(channel.getCategories());
                break;
            case 2 : message = "Ciao!";
                break;
            //case 3 : message = "Sapevi che " + StaticVars2.geoObjectFromDashboard.getName() + " si trova nel tuo comune? Vuoi visitarlo ora?";
            //    break;
            default:
                message = "Ciao!";
        }

        return message;
    }

    private String catMessage (Channel channel){
        String [] concat  = catNameNPoiBuilder (channel.getCategories());
        int rand = randInt(0,concat.length-1);

        String mString = concat[rand];
        String [] split = mString.split("\\|");

        return split[1] + " punti di interesse nella categoria " + split[0] + " ?";
    }

    String [] mArray;
    private String [] catNameNPoiBuilder (mCategory[] categories){
        if(categories != null){
            mArray = new String[categories.length];
            for (int i = 0; i < categories.length; i++){
                mArray[i] = getCatType(""+categories[i].getId()) + "|" +categories[i].getTotalPoi();
            }
        }
        return mArray;
    }

    private static String getCatType(String mCat) {
        if(mCat.equals("100")){
            return "Arte e Cultura";
        }else if(mCat.equals("200")){
            return "Ristorazione e Divertimento";
        }else if(mCat.equals("300")){
            return "Ricettività e Soggiorno";
        }else if(mCat.equals("400")){
            return "Strade e Trasporti";
        }else if(mCat.equals("500")){
            return "Ambiente e Natura";
        }else if(mCat.equals("600")){
            return "Sport e Benessere";
        }else if(mCat.equals("700")){
            return "Emergenze e Soccorso";
        }else if(mCat.equals("800")){
            return "Pubbliche Utilità";
        }else if(mCat.equals("900")){
            return "Cibi e Alimentari";
        }else if(mCat.equals("1000")){
            return "Shopping e Tempo Libero";
        }else if(mCat.equals("1100")){
            return "Servizi Commerciali";
        }else if(mCat.equals("1200")){
            return "Eventi e Manifestazioni";
        }
        return "ND";
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }
}
